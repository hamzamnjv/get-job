import  {useState,useEffect}from 'react'
import axios from 'axios';
import Ecpnprofile from './settings/ecpnpro';
import Ecpnsocial from './settings/ecpnsocial';
import Addmember from './settings/eaddteams';
import AddImage from './settings/addimage';



 function Editcpnprofile() {

    const [userData, setUserData] = useState();
    useEffect(() => {
      // Fetch user data when the component mounts
      const headers = { 'Authorization': 'Bearer my-token' };
      const fetchUserData = async () => {
          try {
              const response = await axios.get('URL_TO_YOUR_API_ENDPOINT',{headers});
              // Assuming your API returns user data in response.data
              setUserData(response.data);
          } catch (error) {
              console.error('Error fetching user data:', error);
          }
      };
  
      fetchUserData();
      return () => {
          
      };
  }, []); 
            
  return (
    <div>
     <Ecpnprofile />
     <Ecpnsocial />
     {/* <Addmember /> */}
     <AddImage />

    </div>
  )
}
export default  Editcpnprofile;