import { useState, useEffect } from 'react';
import categories from '../../../../public/categories.json';
import villes from '../../../../public/villes.json';
import axios from 'axios';

function Ecpnprofile() {
    const [logo, setLogo] = useState(null);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [date_founded, setDate_founded] = useState('');
    const [member_employees, setMember_employees] = useState('');
    const [industry, setIndustry] = useState('');
    const [description, setDescription] = useState('');
    const [ville_id, setVille_id] = useState('');
    const token = localStorage.getItem('token');
    const [userData, setUserData] = useState({});
    const [error, setError] = useState(null);
    const [successMessage, setSuccessMessage] = useState('');


    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            setUserData(JSON.parse(storedUserData));
        }
    }, []);

    useEffect(() => {
        if (userData.user) {
            const { user } = userData;
            setName(user.full_name || '');
            setEmail(user.email || '');
            setLogo(userData?.image || null)
        }
    }, [userData]);

    const handleLogoChange = (e) => {
        setLogo(e.target.files[0]);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        setError(null);
        setSuccessMessage('');
        
    
        // Construct FormData object
        const formData = new FormData();
        formData.append('logo', logo);
        formData.append('name', name);
        formData.append('email', email);
        formData.append('phone', phone);
        formData.append('date_founded', date_founded);
        formData.append('member_employees', member_employees);
        formData.append('industry', industry); 
        formData.append('description', description);
        formData.append('ville_id', ville_id);
        formData.append('user_id', userData.user?.id);
    
        try {
            const response = await axios.post('http://127.0.0.1:8000/api/company/create', formData, {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'multipart/form-data'
                }
            });
            console.log('Response:', response.data);
            setSuccessMessage('Profile updated successfully!');

        } catch (error) {
            console.error('Error:', error);
            setError('Failed to submit the form. Please try again.');
        }
    }
    

    return (
        <div className="bg-white border border-4 rounded-lg shadow relative m-10">
            <div className="flex items-start justify-between p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Settings Profile C.</h3>
            </div>

            <div className="p-6 space-y-6">
                {error && <div className="text-red-600">{error}</div>}
                {successMessage && <div className="text-green-600">{successMessage}</div>}

                <form onSubmit={handleSubmit}>
                    <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="photo-profile" className="text-sm font-medium text-gray-900 block mb-2">Company logo:</label>
                            <img className="w-20 h-20 rounded" src={`http://127.0.0.1:8000/images/profiles/${logo}`} alt="Company logo" />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <input className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block mt-12 w-full p-2.5" id="photo-profile" type="file" accept="image/*" onChange={handleLogoChange} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="name" className="text-sm font-medium text-gray-900 block mb-2">Company Name</label>
                            <input type="text" name="name" id="name" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Company Name" value={name} readOnly />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="email" className="text-sm font-medium text-gray-900 block mb-2">Email</label>
                            <input type="text" name="email" id="email" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Email" value={email} readOnly />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="phone" className="text-sm font-medium text-gray-900 block mb-2">Phone</label>
                            <input type="text" name="phone" id="phone" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Phone number" value={phone} onChange={(e) => setPhone(e.target.value)} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="date_founded" className="text-sm font-medium text-gray-900 block mb-2">Founded</label>
                            <input type="date" name="date_founded" id="date_founded" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" value={date_founded} onChange={(e) => setDate_founded(e.target.value)} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="member_employees" className="text-sm font-medium text-gray-900 block mb-2">Number of Employees</label>
                            <input type="number" name="member_employees" id="member_employees" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" value={member_employees} onChange={(e) => setMember_employees(e.target.value)} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="industry" className="text-sm font-medium text-gray-900 block mb-2">Industry</label>
                            <select name="industry" id="industry" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" value={industry} onChange={(e) => setIndustry(e.target.value)}>
                                <option value=''>Select Industry</option>
                                {categories.map((industry) => (
                                    <option key={industry.id} value={industry.name}>{industry.name}</option>
                                ))}
                            </select>
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="ville_id" className="text-sm font-medium text-gray-900 block mb-2">City</label>
                            <select name="ville_id" id="ville_id" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" value={ville_id} onChange={(e) => setVille_id(e.target.value)}>
                                <option value="">Select a city</option>
                                {villes.map((city) => (
                                    <option key={city.id} value={city.ville}>
                                        {city.ville}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="col-span-6">
                            <label htmlFor="description" className="text-sm font-medium text-gray-900 block mb-2">Description</label>
                            <textarea id="description" name="description" rows="4" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-4" placeholder="Details" value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
                        </div>
                    </div>

                    <div className="p-6 border-t border-gray-200 rounded-b">
                        <button
                            type="submit"
                            className="text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                        >
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Ecpnprofile;
