import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux'; 
import { updateUserData } from '../../../redux/actions';


function ShowImages() {
    const [images, setImages] = useState([]);
    const [loading, setLoading] = useState(true);
    const token = localStorage.getItem('token');
    const userData = useSelector(state => state.userData);
    const dispatch = useDispatch();

    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            const parsedUserData = JSON.parse(storedUserData);
            console.log('Stored userData:', parsedUserData);  
            dispatch(updateUserData(parsedUserData));
        }
    }, [dispatch]);

    useEffect(() => {
        if (!userData || !userData.user || !userData.user.id) {
            console.error('Invalid user data:', userData);  // Debug userData
            setLoading(false);
            return;
        }

        const userId = userData.user.id;
        console.log('Attempting to upload for User ID:', userId);  // Debug user ID

        const fetchImages = async () => {
            try {
                const response = await axios.get('http://127.0.0.1:8000/api/get/images', {
                    params: {
                        user_id: userId
                    },
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                });
                setImages(response.data?.images);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching images:', error);
                setLoading(false);
            }
        };

        fetchImages();
    }, [userData, token]);

    const handleDeleteImage = async (imageId) => {
        try {
            await axios.delete(`/delete/image/${imageId}`);
            setImages(images.filter(image => image.id !== imageId));
        } catch (error) {
            console.error('Error deleting image:', error);
        }
    };

    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <div className="container">
            <h2>All Images</h2>
            <div className='flex flex-wrap '>
            {images.map(image => (
    <div className="max-w-52 m-4 rounded overflow-hidden shadow-lg" key={image.id}>
      <img className="w-full" src={image.image} alt={image.name} />
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{image.name}</div>
      </div>
      <div className="px-6 py-4 flex justify-end">
        <button className="text-red-500 hover:text-red-700" onChange={handleDeleteImage}>
          X
        </button>
      </div>
    </div>
  ))}
        </div></div>
    );
}

export default ShowImages;
