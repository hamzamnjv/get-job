import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserData } from '../../../redux/actions';
import ShowImages from './aShowImage';

function AddImage() {
    const [image, setImage] = useState(null);
    const [uploadStatus, setUploadStatus] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const token = localStorage.getItem('token');
    const userData = useSelector(state => state.userData);
    const dispatch = useDispatch();

    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            const parsedUserData = JSON.parse(storedUserData);
            console.log('Stored userData:', parsedUserData);  // Debug userData
            dispatch(updateUserData(parsedUserData));
        }
    }, [dispatch]);

    const handleImageChange = (event) => {
        setImage(event.target.files[0]);
    };

    const handleUpload = async () => {
        if (!image) {
            setUploadStatus('Please select an image');
            return;
        }

        // Check userData validity before proceeding
        if (!userData || !userData.user || !userData.user.id) {
            setUploadStatus('Invalid user data. Please log in again.');
            console.error('Invalid user data:', userData);  // Debug userData
            return;
        }

        const userId = userData.user.id;
        console.log('Attempting to upload for User ID:', userId);  // Debug user ID

        const formData = new FormData();
        formData.append('image', image);
        formData.append('user_id', userId);

        setIsLoading(true);
        setUploadStatus('');

        try {
            const response = await axios.post('http://127.0.0.1:8000/api/team/images', formData, {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'multipart/form-data'
                }
            });
            console.log('Image uploaded successfully:', response.data);
            setUploadStatus('Image uploaded successfully');
            setImage(null);
        } catch (error) {
            console.error('Error uploading image:', error.response?.data || error.message);
            const errorMessage = error.response?.data?.message || 'Please try again.';
            setUploadStatus(`Error uploading image: ${errorMessage}`);
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <div className="bg-white border border-4 rounded-lg shadow m-10">
            <div className="flex p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Add Image</h3>
            </div>
            <div className="bg-gray-50 py-6 overflow-hidden sm:py-12">
                <label className="block mb-2 ml-8 text-sm font-medium text-gray-900 dark:text-white">
                    Upload image
                </label>
                <input
                    onChange={handleImageChange}
                    accept="image/*"
                    className="block w-4/5 ml-8 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                    id="team_img"
                    type="file"
                />
                <button
                    onClick={handleUpload}
                    className={`mt-8 ml-8 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ${isLoading && 'cursor-not-allowed'}`}
                    disabled={isLoading}
                >
                    {isLoading ? 'Uploading...' : 'Save'}
                </button>
                {uploadStatus && <p className="mt-4 ml-8 text-red-600">{uploadStatus}</p>}
                {image && (
                    <div className="mt-4 ml-8">
                        <p>Selected image: {image.name}</p>
                        <img
                            src={URL.createObjectURL(image)}
                            alt="Preview"
                            className="mt-2 w-32 h-32 object-cover rounded-2xl"
                        />
                    </div>
                )}
            </div>
            <ShowImages />
        </div>
    );
}

export default AddImage;
