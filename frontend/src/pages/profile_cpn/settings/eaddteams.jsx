import React, { useState, useEffect } from 'react';
import axios from 'axios';

function AddMember() {
    const [search, setSearch] = useState('');
    const [open, setOpen] = useState(false);
    const [selectedItems, setSelectedItems] = useState([]);

    {/*  const filteredItems = userData.filter(item =>
        item.Name.toLowerCase().startsWith(search.toLowerCase())
    );   */}   

    const handleInputChange = (event) => {
        setSearch(event.target.value);
        setOpen(true);
    };

    const handleListItemClick = (item) => {
        setSelectedItems(prevItems => [...prevItems, item]);
        setSearch('');
    };

    const handleOutsideClick = (event) => {
        if (event.target.tagName !== 'INPUT') {
            setOpen(false);
        }
    };

    const handleRemoveItem = (itemToRemove) => {
        setSelectedItems(prevItems => prevItems.filter(item => item !== itemToRemove));
    };

    // Function to post data
    const postData = () => {
        axios.post('your_post_endpoint_here', selectedItems)
            .then(response => {
                console.log('Data inserted successfully:', response.data);
                // Optionally, you can clear selectedItems state after successful post
                setSelectedItems([]);
            })
            .catch(error => {
                console.error('Error inserting data:', error);
            });
    };

    return (
        <div className="bg-white border border-4 rounded-lg shadow relative m-10">
            <div className="flex items-start justify-between p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Teams config</h3>
            </div>
            <div className="bg-gray-50 py-6 flex flex-col justify-center relative overflow-hidden sm:py-12">
                <label htmlFor="Search" className="text-sm font-medium text-gray-900 block mb-2">Add Member:</label>
                <input
                    onClick={() => setOpen(!open)}
                    type="search"
                    value={search}
                    onChange={handleInputChange}
                    placeholder="Search Here..."
                    className="py-3 px-4 w-1/2 rounded shadow font-thin focus:outline-none focus:shadow-lg focus:shadow-slate-200 duration-100 shadow-gray-100"
                />

                {open && (
                    <ul
                        onClick={handleOutsideClick}
                        className="w-1/2 relative "
                    >
                        {filteredItems.map((item, index) => (
                            <li
                                key={index}
                                className="w-full text-gray-700 p-4 mt-2 bg-white cursor-pointer"
                                onClick={() => handleListItemClick(item)}
                            >
                                {item.Name}
                            </li>
                        ))}
                    </ul>
                )}
                <div className="mt-2 flex flex-wrap border border-4 rounded-md shadow relative m-10">
                    {selectedItems.map((item, index) => (
                        <div className="each flex rounded shadow w-max text-gray-600 mb-5 bg-white" key={index}>
                            <div className="sec self-center p-2 pr-1">
                                {/* You can adjust the image source based on your data */}
                                <img data="picture" className="h-10 w-10 border p-0.5 rounded-full" src="https://lh3.googleusercontent.com/ogw/ADea4I6N5g9eo7pju00pg3_BF7q6WGS4m6iEzuLJ4iRskA=s32-c-mo" alt="" />
                            </div>

                            <div className="sec self-center p-2 w-64">
                                <div className="flex">
                                    <div className="name text-sm">{item.Name}</div>
                                </div>
                                <div className="title text-xs text-gray-400 -mt-1">{item.Job}</div>
                            </div>

                            <div className="sec self-center p-2 w-2/8">
                                <div className="btn p-2 mr-1 rounded shadow cursor-pointer hover:bg-gray-100">
                                    <button onClick={() => handleRemoveItem(item)}>X</button>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <button onClick={postData} className="bg-green-600 w-1/6 hover:bg-green-800 text-white font-bold py-2 px-4 rounded">save</button>
            </div>
        </div>
    );
}

export default AddMember;
