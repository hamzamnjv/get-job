/* eslint-disable react/prop-types */
import { useState, useEffect } from "react";
import { FaTwitter, FaLinkedin, FaGlobe } from "react-icons/fa";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { updateUserData } from '../../../redux/actions';

export default function Esocial({ res_Data }) {
    const [website, setWebsite] = useState('');
    const [linkedin, setLinkedin] = useState('');
    const [twitter, setTwitter] = useState('');
    const [msg, setMsg] = useState('');
    const [isUpdate, setIsUpdate] = useState(false); 
    const token = localStorage.getItem('token');
    const user = useSelector(state => state.userData);
    const dispatch = useDispatch();

    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        //console.log(storedUserData)
        if (storedUserData) {
            const parsedUserData = JSON.parse(storedUserData);
            dispatch(updateUserData(parsedUserData));
        }
        if (res_Data && res_Data.length > 0 && res_Data[0].social_links) {
            const socialLinks = res_Data[0].social_links;
            setWebsite(socialLinks.website || '');
            setLinkedin(socialLinks.linkedin || '');
            setTwitter(socialLinks.twitter || '');
            setIsUpdate(true); 
        }
    }, [dispatch, res_Data]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const socialLinksData = {
            user_id: user.user.id,
            website,
            twitter,
            linkedin
        };
        try {
            // eslint-disable-next-line no-unused-vars
            const response = isUpdate
                ? await axios.put(`http://localhost:8000/api/SocialLinks/update/${user.user.id}`, socialLinksData, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })
                : await axios.post('http://localhost:8000/api/cp_socialLinks/create', socialLinksData, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                });
            setMsg("Data submitted successfully.");
        } catch (error) {
            setMsg("Error submitting data");
            console.log(error)
            console.error("Error submitting data:", error);
        }
    };

    return (
        <div className="bg-white border border-4 rounded-lg shadow relative m-10">
            <div className="flex items-start justify-between p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Social Media</h3>
            </div>
            <div className="p-6 space-y-6">
                <form onSubmit={handleSubmit}>
                    <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="website" className="text-sm font-medium text-gray-900 block mb-2">Personal Website:</label>
                            <div className="relative mb-6">
                                <div className="absolute inset-y-0 start-0 flex items-center ps-3.5 pointer-events-none">
                                    <FaGlobe />
                                </div>
                                <input type="url" id="website" name="website" value={website} onChange={(e) => setWebsite(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full ps-10 p-2.5" />
                            </div>
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="linkedin" className="text-sm font-medium text-gray-900 block mb-2">LinkedIn:</label>
                            <div className="relative mb-6">
                                <div className="absolute inset-y-0 start-0 flex items-center ps-3.5 pointer-events-none">
                                    <FaLinkedin />
                                </div>
                                <input type="url" id="linkedin" name="linkedin" value={linkedin} onChange={(e) => setLinkedin(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full ps-10 p-2.5" placeholder="https://linkedin.com/in/username" />
                            </div>
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="twitter" className="text-sm font-medium text-gray-900 block mb-2">Twitter:</label>
                            <div className="relative mb-6">
                                <div className="absolute inset-y-0 start-0 flex items-center ps-3.5 pointer-events-none">
                                    <FaTwitter />
                                </div>
                                <input type="url" id="twitter" name="twitter" value={twitter} onChange={(e) => setTwitter(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full ps-10 p-2.5" placeholder="https://twitter.com/username" />
                            </div>
                        </div>
                    </div>
                    <div className="p-6 border-t mt-4 border-gray-200 rounded-b">
                        <button className="text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">
                            Save
                        </button>
                    </div>
                </form>
                {msg && (
                    <div className={`p-4 mb-4 text-sm rounded-lg ${msg.includes('Error') ? 'text-red-800 bg-red-200' : 'text-green-800 bg-green-200'}`} role="alert">
                        <span className="font-medium">{msg}</span>
                    </div>
                )}
            </div>
        </div>
    );
}
