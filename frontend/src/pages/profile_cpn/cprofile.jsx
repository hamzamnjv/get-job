import React, { useState, useEffect } from "react";
import axios from "axios";
import Editcpn from "./utils/editcpn";
import Cardcpn from "./utils/cardcpn";
import Aboutcpn from "./utils/aboutcpn";
import Imagecpn from "./utils/imagecpn";
import PostList from "./utils/postlist";
// import Teamcpn from "./utils/teamcpn";

function Profile() {
    const [res_Data, setRes_Data] = useState(() => {
        // Retrieve data from local storage if available
        const storedData = localStorage.getItem('res_Data');
        return storedData ? JSON.parse(storedData) : null;
    });

    const token = localStorage.getItem('token');

    useEffect(() => {
        const headers = { 'Authorization': `Bearer ${token}` };

        const fetchRes_Data = async () => {
            try {
                const response = await axios.get('http://127.0.0.1:8000/api/company/data', { headers });
                setRes_Data(response.data);
                // Store data in local storage
                localStorage.setItem('res_Data', JSON.stringify(response.data));
            } catch (error) {
                console.error('Error fetching user data:', error);
            }
        };

        fetchRes_Data();

        return () => {
            // Cleanup function
        };
    }, [token]);

    return (
        <>
            <Editcpn />
            <Cardcpn res_Data={res_Data} />
            <Aboutcpn res_Data={res_Data} />
            <Imagecpn res_Data={res_Data} />
            <PostList res_Data={res_Data} />
            {/* <Teamcpn res_Data={res_Data} /> */}
        </>
    );
}

export default Profile;
