import  { useEffect, useState } from 'react';
import axios from 'axios';
import categories from '../../../public/categories.json';
import villes from '../../../public/villes.json';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserData } from '../../redux/actions';


export default function Postjob() {
    const [title, setTitle] = useState('');
    const [salary, setSalary] = useState('');
    const [category_id, setCategory_id] = useState('');
    const [description, setDescription] = useState('');
    const [ville_id, setVille_id] = useState('');
    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');
    const token = localStorage.getItem('token');
    const userData = useSelector(state => state.userData);
    const dispatch = useDispatch();


    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
          const userData = JSON.parse(storedUserData);
          dispatch(updateUserData(userData));    
        }
      }, [dispatch]);


    

    const handleSubmit = async (event) => {
        event.preventDefault();
        
        // Check if required fields are filled
        if (!title || !salary || !category_id || !description || !ville_id) {
            setError('Please fill in all fields.');
            return;
        }

        const formData = {
            user_id : userData.user?.id,
            title,
            salary,
            category_id,
            description,
            ville_id
        };

        try {
            // Replace 'your-api-endpoint' with your actual API endpoint
            const response = await axios.post('http://127.0.0.1:8000/api/post/create', formData, {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'multipart/form-data'
                }
            });
            console.log(response.data);
            setSuccess('Job posted successfully!');
            setError('');

        } catch (error) {
            setError('Error posting form data. Please try again later.');
            console.error('Error posting form data:', error);
            setSuccess('');

            // Handle error gracefully, e.g., show error message to the user
        }
    };

    return (
        <div className="bg-white border border-4 rounded-lg shadow relative m-10">
            <div className="flex items-start justify-between p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Post Job</h3>
            </div>
            <div className="p-6 space-y-6">
                <form onSubmit={handleSubmit}>
                    <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="first-name" className="text-sm font-medium text-gray-900 block mb-2">Job Title</label>
                            <input type="text" name="first-name" id="first-name" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="First Name" value={title} onChange={(e) => setTitle(e.target.value)} />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="Salary" className="text-sm font-medium text-gray-900 block mb-2">Salary</label>
                            <input type="text" name="Salary" id="Salary" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" value={salary} onChange={(e) => setSalary(e.target.value)} />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="category_id" className="text-sm font-medium text-gray-900 block mb-2">Category_id</label>
                            <select name="category_id" id="category_id" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" value={category_id} onChange={(e) => setCategory_id(e.target.value)}>
                            <option value="">Select Industry</option>
                                        {categories.map((category) => (
                                            <option key={category.id} value={category.name}>{category.name}</option>
                                        ))}
                            </select>
                        </div>

                        
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="ville_id" className="text-sm font-medium text-gray-900 block mb-2">City:</label>
                            <select value={ville_id} onChange={(e) => setVille_id(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                                <option value="">Select a city</option>
                                {villes.map((city) => (
                                    <option key={city.id} value={city.ville}>
                                        {city.ville}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="col-span-full">
                            <label htmlFor="Description" className="text-sm font-medium text-gray-900 block mb-2">Description</label>
                            <textarea id="Description" rows="2" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-4" placeholder="description" value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
                        </div>
                    </div>
                    <div className="p-6 border-t border-gray-200 rounded-b">
                        <button className="text-white bg-green-600 hover:bg-green-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">Save</button>
                            <br /><br />
                        {error && <div className="text-red-500">{error}</div>}
                        {success && <div className="text-green-500">{success}</div>}
                    </div>
                </form>
            </div>
        </div>
    )
}
