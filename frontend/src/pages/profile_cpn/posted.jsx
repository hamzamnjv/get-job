import  { useState, useEffect } from "react";
import axios from 'axios';


const Listposted = () => {

    const [res_Data, setRes_Data] = useState(() => {
        // Retrieve data from local storage if available
        const storedData = localStorage.getItem('res_Data');
        return storedData ? JSON.parse(storedData) : null;
    });

    const token = localStorage.getItem('token');

    useEffect(() => {
        const headers = { 'Authorization': `Bearer ${token}` };

        const fetchRes_Data = async () => {
            try {
                const response = await axios.get('http://127.0.0.1:8000/api/list/posted', { headers });
                setRes_Data(response.data.savedPosts);
                console.log(res_Data)
                // Store data in local storage
                localStorage.setItem('res_Data', JSON.stringify(response.data));
            } catch (error) {
                console.error('Error fetching user data:', error);
            }
        };

        fetchRes_Data();

        return () => {
            // Cleanup function
        };
    }, [token]);

    
    return (
        <>
        <div className="min-h-screen bg-gray-100 py-5">
            <div className='overflow-x-auto w-full'>
                <table className='mx-auto max-w-4xl w-full whitespace-nowrap rounded-lg bg-white divide-y divide-gray-300 overflow-hidden'>
                    <thead className="bg-gray-900">
                        <tr className="text-white text-left">
                            <th className="font-semibold text-sm uppercase px-6 py-4"> Offer Name </th>
                            <th className="font-semibold text-sm uppercase px-6 py-4"> Candidate </th>
                            <th className="font-semibold text-sm uppercase px-6 py-4 text-center"> Email </th>
                            <th className="font-semibold text-sm uppercase px-6 py-4 text-center"> date posted </th>
                            <th className="font-semibold text-sm uppercase px-6 py-4"> Profile</th>
                        </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200">
                        {Array.isArray(res_Data) &&res_Data.map(e => (
                            <tr key={e.post?.id}>
                                <td className="px-6 py-4">
                                    <div>
                                        <p className="text-gray-500 text-sm font-semibold tracking-wide">{e.post?.title}</p>
                                    </div>
                                </td>

                                <td className="px-6 py-4">
                                    <p className="text-gray-500 text-sm font-semibold tracking-wide">{e.user?.full_name}</p>
                                </td>

                                <td className="px-6 py-4 text-center">
                                    <span className={`text-white text-sm w-1/3 pb-1 bg-green-600 font-semibold px-2 rounded-full`}>
                                        {e.user?.email}
                                    </span>
                                </td>

                                <td className="px-6 py-4 text-center">{e.created_at.split('T')[0]}</td>

                                <td className="px-6 py-4 text-center">
                                    <div className="flex items-center space-x-3">
                                        <div className="inline-flex w-10 h-10">
                                            <img className='w-10 h-10 object-cover rounded-full' alt='User avatar' 
                                            src={`http://127.0.0.1:8000/images/profiles/${e.user?.profile?.image}`} />
                                        </div>
                                        <a href="#" className="text-purple-800 hover:underline">View</a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>

   </> );
};

export default Listposted;
