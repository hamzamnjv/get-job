import React from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel';



export default function Imagecpn({res_Data}) {


  return (
    
<div className="flex justify-center mt-8">
  <div className=" bg-white border border-gray-200 rounded-lg shadow hover:shadow-none shadow-lg w-3/6">
    <div>  
      <h1 className="text-2xl bg-gray-200 font-bold text-center">Images</h1>
      <Carousel className="mt-10">
         {res_Data[0]?.teamimages.map(img => ( 
        <div key={img.id} ><img src={`http://127.0.0.1:8000/${img.image_path}`} /><p className="legend">{/*{userData.imageTitle}*/}Galory</p></div>
           ))}   
       
      </Carousel>
    </div>
  </div>
</div>
  )
}

