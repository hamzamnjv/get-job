import React from "react";
import { FaTwitter, FaLinkedin, FaGlobe } from 'react-icons/fa';


export default function Aboutcpn({ res_Data }) {
    return (
        <div className="flex flex-col md:flex-row bg-white border border-gray-200 rounded-lg shadow mt-4 w-full max-w-screen-xl mx-auto hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
            
            <div className="w-full md:w-3/4 mb-4 md:mb-0">
                <h1 className="text-2xl bg-gray-200 font-bold text-center">About</h1>
                {res_Data ? (
                    <div>
                        <p className="text-lg py-4 font-mono  font-semibold m-auto text-center">
                            {res_Data[0]?.description}
                        </p>
                    </div>
                ) : (
                    <p className="mx-auto text-center">Loading...</p>
                )}
            </div>


            <div className="w-full md:w-1/4">
                
                <div className="bg-gray-200 px-4 lg:text-center">
                    
                    <h2 className="text-2xl bg-gray-200 font-bold text-center">
                        Social Media
                    </h2>
                </div>
                <div className="px-4 py-5 sm:p-6 hover:bg-gray-50 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
                     {res_Data[0]?.social_links?.linkedin && ( 
                    <div className="flex flex-col items-start justify-between mb-6">
                        <span className="text-sm font-medium text-gray-600" aria-label="LinkedIn">
                        <FaLinkedin className="h-8 w-8 text-gray-600" />
                        </span>
                        <span className="text-sm font-medium text-gray-800">
                             {res_Data[0]?.social_links?.linkedin}
                        </span>
                    </div>
                     )} 

                     {res_Data[0]?.social_links?.twitter && ( 
                    <div className="flex flex-col items-start justify-between mb-6">
                        <span className="text-sm font-medium text-gray-600" aria-label="Twitter">
                    <FaTwitter className="h-8 w-8 text-gray-600" />
                     </span>
                        <span className="text-sm font-medium text-gray-800">
                             {res_Data[0]?.social_links?.twitter} 
                        </span>
                    </div>
                     )}

                     {res_Data[0]?.social_links?.website && ( 
                    <div className="flex flex-col items-start justify-between mb-6">
                        <span className="text-sm font-medium text-gray-600" aria-label="Website">
                        <FaGlobe className="h-8 w-8 text-gray-600" />
                        </span>
                        <span className="text-sm font-medium text-gray-800">
                            {res_Data[0]?.social_links?.website} 
                        </span>
                    </div>
                    )} 
                </div>
            </div>
        </div>
    );
}
