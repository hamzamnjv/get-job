import axios from 'axios';
import { useEffect, useState } from 'react';
import categories from '../../../public/categories.json';
import villes from '../../../public/villes.json';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserData } from '../../redux/actions';
import { AiOutlineSearch, AiOutlineEnvironment, AiOutlineBank, AiOutlineAppstoreAdd } from "react-icons/ai";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function SearchJob() {
    const [searchData, setSearchData] = useState({
        title: '',
        ville: '',
        company: '',
        category: ''
    });

    const [searchResults, setSearchResults] = useState([]);
    const [loading, setLoading] = useState(false);
    const token = localStorage.getItem('token');
    const userData = useSelector(state => state.userData);
    const dispatch = useDispatch();

    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            const userData = JSON.parse(storedUserData);
            dispatch(updateUserData(userData));
            setSearchData(prevSearchData => ({
                ...prevSearchData,
                user_id: userData.user.id
            }));
        }
        fetchData();
    }, [dispatch]);

    const fetchData = async () => {
        try {
            const response = await axios.get('http://127.0.0.1:8000/api/posts/all', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            setSearchResults(response.data.allposts);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const handleSearch = async (e) => {
        e.preventDefault();
        setLoading(true);

        try {
            const response = await axios.post('http://127.0.0.1:8000/api/recherche/posts', searchData, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            setSearchResults(response.data.filterRechercheCompanies);
            setLoading(false);
        } catch (error) {
            console.error('Error searching:', error);
            setLoading(false);
        }
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setSearchData(prevSearchData => ({
            ...prevSearchData,
            [name]: value
        }));
    };

    const handleApply = async (jobId) => {
        const storedUserData = localStorage.getItem('userData');
        const userData = storedUserData ? JSON.parse(storedUserData) : null;
        const userId = userData ? userData.user.id : null;

        if (!userId) {
            toast.error("User not logged in");
            return;
        }

        try {
            const response = await axios.post('http://127.0.0.1:8000/api/user/posted', { 
                user_id: userId, 
                post_id: jobId 
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            toast.success("Offer Sent Successfully");
        } catch (error) {
            console.error("Error applying for job:", error);
            toast.error("Failed to apply for the job. Please try again later.");
        }
    };

    return (
        <div className="searchDiv grid gap-10 p-6 md:p-12 bg-sky-100">
            <ToastContainer />
            <div className="text-center md:text-left align-text-top font-sans font-custom text-3xl md:text-4xl">
                Search For <strong className='text-green-600'>Job!</strong>
            </div>

            <form onSubmit={handleSearch}>
                <div className="firstDiv flex flex-col md:flex-row justify-between items-center rounded-[8px] gap-5 bg-white p-4 md:p-5 shadow-lg shadow-gray-700">

                    <div className="flex items-center gap-2 w-full md:w-1/4">
                        <AiOutlineSearch className="text-2xl cursor-pointer" />
                        <input type="text" name="title" value={searchData.title} onChange={handleInputChange} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Search job here ..." />
                    </div>

                    <div className="flex items-center gap-2 w-full md:w-1/4">
                        <AiOutlineEnvironment className="text-2xl cursor-pointer" />
                        <select name="ville" value={searchData.ville} onChange={handleInputChange} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                            <option value="">Select a ville</option>
                            {villes.map((ville) => (
                                <option key={ville.id} value={ville.ville}>
                                    {ville.ville}
                                </option>
                            ))}
                        </select>
                    </div>

                    <div className="flex items-center gap-2 w-full md:w-1/4">
                        <AiOutlineBank className="text-2xl cursor-pointer" />
                        <input type="text" name="company" value={searchData.company} onChange={handleInputChange} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Search by company ..." />
                    </div>

                    <div className="flex items-center gap-2 w-full md:w-1/4">
                        <AiOutlineAppstoreAdd className="text-2xl cursor-pointer" />
                        <select name="category" value={searchData.category} onChange={handleInputChange} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                            <option value="">Select category</option>
                            {categories.map((category) => (
                                <option key={category.id} value={category.name}>{category.name}</option>
                            ))}
                        </select>
                    </div>

                    <button type="submit" className="bg-green-600 hover:bg-blue-200 text-white font-semibold py-2 px-4 rounded mt-4 md:mt-0">Search</button>
                </div>
            </form>

            <div className="bg-white border border-4 rounded-lg shadow relative m-10">
                <div className="flex items-start justify-between p-5 border-b rounded-t">
                    <h3 className="text-xl font-semibold">Results:</h3>
                </div>

                {loading && <p>Loading...</p>}
                {searchResults.length > 0 ? (
                    searchResults.map(job => (
                        <div key={job.id} className="bg-slate-100 text-slate-900 rounded-lg p-4 m-2">
                            <div className="flex space-x-2 items-center">
                                <div className="w-20 h-20">
                                    <img alt="avatar" src='https://static.vecteezy.com/system/resources/previews/015/280/523/non_2x/job-logo-icon-with-tie-image-free-vector.jpg' className="rounded-full w-full h-full object-cover"/>
                                </div>
                                <div className="">
                                    <div className="flex flex-col ml-12">
                                        <div className="flex my-1"><p className="text-sm font-bold ">Job Title :</p> <span className="ml-2 font-semibold">{job.title}</span></div>
                                        <div className="flex my-1"><p className="text-sm font-bold "> Date :</p> <span className="ml-2 font-semibold">{job.created_at?.split('T')[0]}</span></div>
                                        <div className="flex my-1"><p className="text-sm font-bold ">Company :</p> <span className="ml-2 font-semibold">{job.company?.name}</span></div>
                                        <div className="flex my-1"><p className="text-sm font-bold "> Category :</p> <span className="ml-2 font-semibold">{job.category?.name }</span></div>
                                        <div className="flex my-1"><p className="text-sm font-bold "> Ville :</p> <span className="ml-2 font-semibold">{job.ville?.name}</span></div>
                                        <div className="flex my-1"><p className="text-sm font-bold "> Salary :</p> <span className="ml-2 font-semibold">{job.salary}</span></div>
                                        <div className="flex my-1"><p className="text-sm font-bold "> Description :</p> <span className="ml-2 font-semibold">{job.description}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div className="flex justify-end">
                                <button 
                                    className="text-white bg-green-600 hover:bg-green-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-4 py-2.5" 
                                    type="button" 
                                    onClick={() => handleApply(job.id)}
                                >
                                    Postuler
                                </button>
                            </div>
                        </div>
                    ))
                ) : (
                    <p className='text-center my-4 font-bold'>No results found.</p>
                )}
            </div>
        </div>
    );
}
