import axios from 'axios';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import categories from '../../../public/categories.json';
import villes from '../../../public/villes.json';
import { useDispatch } from 'react-redux';
import { updateUserData } from '../../redux/actions';
import { AiOutlineSearch, AiOutlineEnvironment, AiOutlineBank, AiOutlineAppstoreAdd } from "react-icons/ai";

export default function SearchEmployee() {
  const [userId, setUserId] = useState(null);
  const [searchData, setSearchData] = useState({
    title: '',
    ville: '',
    user_name: '',
    category: '',
    user_id: null
  });

  const [searchResults, setSearchResults] = useState([]);
  const [loading, setLoading] = useState(false);
  const token = localStorage.getItem('token');
  const dispatch = useDispatch();

  useEffect(() => {
    const storedUserData = localStorage.getItem('userData');
    if (storedUserData) {
      const parsedUserData = JSON.parse(storedUserData);
      setUserId(parsedUserData.user.id);
      dispatch(updateUserData(parsedUserData));
      setSearchData(prevSearchData => ({
        ...prevSearchData,
        user_id: parsedUserData.user.id
      }));
      
      
      // Ensure fetchData is called after user_id is set
      if (parsedUserData.user.id) {
        
        fetchData(parsedUserData.user.id);
      }
    }
  }, [dispatch]);

  const fetchData = async (userId) => {
    setLoading(true);
    try {
      const response = await axios.get('http://127.0.0.1:8000/api/profiles/alls', {
        params: { ...searchData, user_id: userId },
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      });
      console.log(response.data.filterPostsAll);

      setSearchResults(response.data.filterPostsAll);
    } catch (error) {
      console.error('Error fetching data:', error);
    } finally {
      setLoading(false);
    }
  };

  const handleSearch = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const response = await axios.post('http://127.0.0.1:8000/api/profil/alls', {
        ...searchData,
        user_id: userId // Ensure user_id is set here
      }, {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      });
      setSearchResults(response.data.filterPostsAll);
      
    } catch (error) {
      console.error('Error searching:', error);
    } finally {
      setLoading(false);

    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setSearchData(prevSearchData => ({
      ...prevSearchData,
      [name]: value
    }));
  };

  return (
    <>
      <div className="searchDiv grid gap-10 p-6 md:p-12 bg-sky-100">
        <div className="text-center md:text-left font-sans text-3xl md:text-4xl">
          Search For <strong className='text-green-600'>Employee!</strong>
        </div>
        <form onSubmit={handleSearch}>
          <div className="firstDiv flex flex-col md:flex-row justify-between items-center rounded-8 gap-5 bg-white p-4 md:p-5 shadow-lg">
            <div className="flex items-center gap-2 w-full md:w-1/4">
              <AiOutlineSearch className="text-2xl" />
              <input 
                type="text" 
                name="user_name" 
                value={searchData.user_name} 
                onChange={handleInputChange} 
                className="shadow-sm bg-gray-50 border border-gray-300 rounded-lg w-full p-2.5" 
                placeholder="Search name Employee ..." 
              />
            </div>
            <div className="flex items-center gap-2 w-full md:w-1/4">
              <AiOutlineEnvironment className="text-2xl" />
              <select 
                name="ville" 
                value={searchData.ville} 
                onChange={handleInputChange} 
                className="shadow-sm bg-gray-50 border border-gray-300 rounded-lg w-full p-2.5"
              >
                <option value="">Select a ville</option>
                {villes.map((ville) => (
                  <option key={ville.id} value={ville.ville}>{ville.ville}</option>
                ))}
              </select>
            </div>
            <div className="flex items-center gap-2 w-full md:w-1/4">
              <AiOutlineBank className="text-2xl" />
              <input 
                type="text" 
                name="title" 
                value={searchData.title} 
                onChange={handleInputChange} 
                className="shadow-sm bg-gray-50 border border-gray-300 rounded-lg w-full p-2.5" 
                placeholder="Search by Title Employee ..." 
              />
            </div>
            <div className="flex items-center gap-2 w-full md:w-1/4">
              <AiOutlineAppstoreAdd className="text-2xl" />
              <select 
                name="category" 
                value={searchData.category} 
                onChange={handleInputChange} 
                className="shadow-sm bg-gray-50 border border-gray-300 rounded-lg w-full p-2.5"
              >
                <option value="">Select Industry</option>
                {categories.map((category) => (
                  <option key={category.id} value={category.name}>{category.name}</option>
                ))}
              </select>
            </div>
            <button 
              type="submit" 
              className="bg-green-600 text-white font-semibold py-2 px-4 rounded mt-4 md:mt-0 hover:bg-green-700"
            >
              Search
            </button>
          </div>
        </form>
      </div>
      <div className="bg-white border-4 rounded-lg shadow m-10 p-5">
        <div className="border-b pb-5 mb-5">
          <h3 className="text-xl font-semibold">Result :</h3>
        </div>
        {loading && <p>Loading...</p>}
        <div className="flex flex-wrap justify-center">
        { searchResults.length > 0 ? (
          searchResults.map(user => (
            
            <div key={user.id} className=" mb-5 mx-8">
              <div className=" max-w-xs">
                <div className="bg-white shadow-xl rounded-lg py-3">
                  <div className="p-2">
                    <img 
                      className="w-32 h-32 rounded-full mx-auto" 
                      src={`http://127.0.0.1:8000/images/profiles/${user.image}`} 
                      alt="User Avatar" 
                    />
                  </div>
                  <div className="p-2 text-center">
                    <h3 className="text-xl text-gray-900 font-medium leading-8">{user.user.full_name}</h3>
                    <p className="text-gray-400 text-sm font-semibold">{user.category?.name}</p>
                    <h5 className=" text-gray-600 font-medium leading-8">{user.profile_title}</h5>
                    <p className="text-gray-400 text-xs font-semibold">{user.ville?.name}</p>

                    <Link 
                      className="text-xs text-indigo-500 italic hover:underline hover:text-indigo-600 font-medium" 
                      to={`/profile/View/${user.id}`}
                    >
                      View Profile
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <p className="text-center font-bold">No results found.</p>
        )}</div>
      </div>
    </>
  );
}
