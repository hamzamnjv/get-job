import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import categories from '../../../public/categories.json';
import villes from '../../../public/villes.json';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserData } from '../../redux/actions';
import { AiOutlineSearch, AiOutlineEnvironment, AiOutlineAppstoreAdd } from "react-icons/ai";

export default function SearchCompany() {
    const [searchData, setSearchData] = useState({
        name: '',
        ville: '',
        category: ''
    });

    const [searchResults, setSearchResults] = useState([]);
    const [loading, setLoading] = useState(false);
    const token = localStorage.getItem('token');
    const dispatch = useDispatch();

    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            const userData = JSON.parse(storedUserData);
            dispatch(updateUserData(userData));
            setSearchData(prevSearchData => ({
                ...prevSearchData,
                user_id: userData.user.id
            }));
        }
        fetchData();
    }, [dispatch]);

    const fetchData = async () => {
        try {
            const response = await axios.get('http://127.0.0.1:8000/api/company/alls', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            setSearchResults(response.data.allCompanies);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const handleSearch = async (e) => {
        e.preventDefault();
        setLoading(true);

        try {
            const response = await axios.post('http://127.0.0.1:8000/api/recherche/companys', searchData, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            setSearchResults(response.data.filterRechercheCompanies);
            setLoading(false);
        } catch (error) {
            console.error('Error searching:', error);
            setLoading(false);
        }
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setSearchData(prevSearchData => ({
            ...prevSearchData,
            [name]: value
        }));
    };

    return (
        <>
            <div className="searchDiv grid gap-10 p-6 md:p-12 bg-sky-100">
                <div className="text-center md:text-left align-text-top font-sans font-custom text-3xl md:text-4xl">
                    Search For <strong className='text-green-600'>Entreprise!</strong>
                </div>

                <form onSubmit={handleSearch}>
                    <div className="firstDiv flex flex-col md:flex-row justify-between items-center rounded-[8px] gap-5 bg-white p-4 md:p-5 shadow-lg shadow-gray-700">
                        <div className="flex items-center gap-2 w-full md:w-1/4">
                            <AiOutlineSearch className="text-2xl cursor-pointer" />
                            <input
                                type="text"
                                name="name"
                                value={searchData.name}
                                onChange={handleInputChange}
                                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                placeholder="Search name entreprise ..."
                            />
                        </div>

                        <div className="flex items-center gap-2 w-full md:w-1/4">
                            <AiOutlineEnvironment className="text-2xl cursor-pointer" />
                            <select
                                name="ville"
                                value={searchData.ville}
                                onChange={handleInputChange}
                                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                            >
                                <option value="">Select a ville</option>
                                {villes.map((ville) => (
                                    <option key={ville.id} value={ville.ville}>
                                        {ville.ville}
                                    </option>
                                ))}
                            </select>
                        </div>


                        <div className="flex items-center gap-2 w-full md:w-1/4">
                            <AiOutlineAppstoreAdd className="text-2xl cursor-pointer" />
                            <select
                                name="category"
                                value={searchData.category}
                                onChange={handleInputChange}
                                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                            >
                                <option value="">Select Industry</option>
                                {categories.map((category) => (
                                    <option key={category.id} value={category.name}>
                                        {category.name}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <button type="submit" className="bg-green-600 hover:bg-blue-200 text-white font-semibold py-2 px-4 rounded mt-4 md:mt-0">Search</button>
                    </div>
                </form>
            </div>

            <div className="bg-white border border-4 rounded-lg shadow relative m-10">
                <div className="flex items-start justify-between p-5 border-b rounded-t">
                    <h3 className="text-xl font-semibold">Result :</h3>
                </div>

                {loading && <p>Loading...</p>}
                <div className="flex flex-wrap ">
                    {searchResults.map(cpn => (
                        <div key={cpn.id} className="flex mx-8 my-8">
                            <div className="max-w-xs">
                                <div className="bg-white shadow-xl rounded-lg py-3">
                                    <div className="photo-wrapper p-2">
                                        <img
                                            className="w-32 h-32 rounded-full mx-auto"
                                            src={`http://127.0.0.1:8000/images/profiles/${cpn.logo}`}
                                            alt="User Avatar"
                                        />
                                    </div>
                                    <div className="p-2">
                                        <h3 className="text-center text-xl text-gray-900 font-medium leading-8">{cpn.name}</h3>
                                        <div className="text-center text-gray-400 text-xs font-semibold">
                                            <p>{cpn.category?.name}</p>
                                            <p className='text-emerald-900'>{cpn.ville?.name}</p>
                                        </div>
                                        <div className="text-center my-3">
                                            <Link className="text-xs text-indigo-500 italic hover:underline hover:text-indigo-600 font-medium" to={`/company/View/${cpn.id}`}>View Profile</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}
