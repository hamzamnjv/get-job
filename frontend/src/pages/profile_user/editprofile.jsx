import  {useState,useEffect}from 'react'
import axios from 'axios';
import Eprofile from './setting/eprofile';
import Eexperience from './setting/eexperience';
import Esocial from './setting/esocial';
import Eformation from './setting/Eformation';
import Eskills from './setting/eskills';
// import { useSelector } from 'react-redux';


 function Editprofile() {

    const [res_Data, setRes_Data] = useState();
    // const res_Datas = useSelector(state => state.res_Data);
    const token =localStorage.getItem('token');

    useEffect(() => {
      // Fetch user data when the component mounts
      const headers = { 'Authorization': `Bearer ${token}` };
      const fetchRes_Data = async () => {
          try {
              const response = await axios.get('http://127.0.0.1:8000/api/employee/data',{headers});
              // Assuming your API returns user data in response.data
              setRes_Data(response.data);
          } catch (error) {
              console.error('Error fetching user data:', error);
          }
      };
  
      fetchRes_Data();
      return () => {
          
      };
  }, [  token]); 



  const [cpn_Data, setcpn_Data] = useState();
// const cpn_Datas = useSelector(state => state.cpn_Data);
useEffect(() => {
  // Fetch user data when the component mounts
  const headers = { 'Authorization': `Bearer ${token}` };
  const fetchcpn_Data = async () => {
      try {
          const response = await axios.get('http://127.0.0.1:8000/api/all/cpn',{headers});
          // Assuming your API returns user data in response.data
          setcpn_Data(response.data);
      } catch (error) {
          console.error('Error fetching user data:', error);
      }
  };

  fetchcpn_Data();

  // Clean up function
  return () => {
      // Perform cleanup if needed
  };
}, [token]);





  return (
    <div>
      <Eprofile res_Data={res_Data} />
      <Eexperience res_Data={res_Data} />
      <Esocial res_Data={res_Data}/>
      <Eformation res_Data={res_Data}  />
      <Eskills res_Data={res_Data} />
    </div>
  )
}
export default  Editprofile;