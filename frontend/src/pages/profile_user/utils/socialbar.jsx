import { useState, useEffect } from 'react';
import { FaTwitter, FaLinkedin, FaGlobe } from 'react-icons/fa';

export default function Socialbar({ res_Data }) {
  const [website, setWebsite] = useState('');
  const [linkedin, setLinkedin] = useState('');
  const [twitter, setTwitter] = useState('');

  useEffect(() => {
    if (res_Data && res_Data.length > 0) {
      const { website, linkedin, twitter } = res_Data[0]?.social_links || {};

      setWebsite(website || '');
      setLinkedin(linkedin || '');
      setTwitter(twitter || '');
    }
  }, [res_Data]);
  const shouldRender = (res_Data && res_Data.length > 0);

  return (
    shouldRender && (
    <div className="max-w-screen w-80 mx-auto bg-white rounded-lg shadow-md overflow-hidden mt-12 lg:float-right lg:mr-12">
      <div className="bg-gray-100 px-4 py-2 lg:text-center">
        <h2 className="text-lg font-medium text-gray-800">Social Media </h2>
      </div>
      <div className="px-4 py-5 sm:p-6">
        {linkedin && (
          <div className="flex flex-col items-start justify-between mb-6">
            <span className="text-sm font-medium text-gray-600">
              <FaLinkedin className="h-8 w-8 text-gray-600" />
            </span>
            <span className="text-sm font-medium text-gray-800">{linkedin}</span>
          </div>
        )}

        {twitter && (
          <div className="flex flex-col items-start justify-between mb-6">
            <span className="text-sm font-medium text-gray-600">
              <FaTwitter className="h-8 w-8 text-gray-600" />
            </span>
            <span className="text-sm font-medium text-gray-800">{twitter}</span>
          </div>
        )}

        {website && (
          <div className="flex flex-col items-start justify-between mb-6">
            <span className="text-sm font-medium text-gray-600">
              <FaGlobe className="h-8 w-8 text-gray-600" />
            </span>
            <span className="text-sm font-medium text-gray-800">{website}</span>
          </div>
        )}
      </div>
    </div>
  ));
}
