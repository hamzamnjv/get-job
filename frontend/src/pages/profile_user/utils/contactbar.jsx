import { useEffect, useState } from "react";
import { MdOutlineTranslate, MdAttachEmail } from "react-icons/md";
import { FaPhoneAlt } from "react-icons/fa";

export default function Contactbar({ res_Data }) {
  const [userData, setUserData] = useState({});
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [userLanguages, setUserLanguages] = useState([]);

  useEffect(() => {
    const storedUserData = localStorage.getItem('userData');
    if (storedUserData) {
      const parsedUserData = JSON.parse(storedUserData);
      setUserData(parsedUserData);
      setEmail(parsedUserData?.user?.email || '');
    }
  }, []);

  useEffect(() => {
    if (res_Data && res_Data.length > 0) {
      const languagesArray = JSON.parse(res_Data[0]?.languages || '[]');
      const { phone } = res_Data[0] || {};

      setPhone(phone || '');
      setUserLanguages(languagesArray || []);
    }
  }, [res_Data]);

  const shouldRender = ( userData && userData.length > 0 )|| (res_Data && res_Data.length > 0);

  return (
    shouldRender && (
      <div className="max-w-screen w-80 mx-auto bg-white rounded-lg shadow-md overflow-hidden mt-12 lg:float-right lg:mr-12">
        <div className="bg-gray-100 px-4 py-2 lg:text-center">
          <h2 className="text-lg font-medium text-gray-800">Additional Details</h2>
        </div>

        <div className="px-4 py-5 sm:p-6">
          {email && (
            <div className="flex items-start mb-6">
              <span className="text-2xl font-medium text-gray-600">
                <MdAttachEmail />
              </span>
              <span className="text-lg ml-4 font-medium text-gray-800">{email}</span>
            </div>
          )}

          {phone && (
            <div className="flex items-start mb-6">
              <FaPhoneAlt className="text-2xl" />
              <p className="text-lg ml-4 font-medium text-gray-800">{phone}</p>
            </div>
          )}

          {userLanguages.length > 0 && (
            <div className="flex items-start mb-6">
              <span className="text-2xl font-medium text-gray-600">
                <MdOutlineTranslate />
              </span>
              <span className="text-lg ml-4 flex list-none font-medium text-gray-800">
                {userLanguages.map(language => (
                  <li key={language}>
                    {language} &nbsp;
                  </li>
                ))}
              </span>
            </div>
          )}
        </div>
      </div>
    )
  );
}
