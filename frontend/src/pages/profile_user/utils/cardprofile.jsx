/* eslint-disable react/prop-types */
import { useState,useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Cardprofile({res_Data ,isOwnProfile }) {
    const [userName, setUserName] = useState('');
    const [image, setImage] = useState('');
    const [title, setTitle] = useState('');
    const [ville_name, setVille_name] = useState('');
    const [userData, setUserData] = useState({}); 


    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            setUserData(JSON.parse(storedUserData));
        }
    }, []);
    useEffect(() => {
        
        if (res_Data && res_Data.length > 0 && userData) {
            const { profile_title, image,ville } = res_Data[0];

            setUserName(userData?.user?.full_name || '');
            setTitle(profile_title || '');
            setImage(image || null);
            setVille_name(ville?.name || '');
        }
    }, [ res_Data]);


    if (!res_Data) {
        return <div>Loading...</div>; 
    }

    return (
        <div className="card border hover:shadow-none relative flex flex-col shadow-lg  m-10">
            <img
                className="max-h-20 w-full opacity-80 absolute top-0"
                style={{ zIndex: -1 }}
                src="https://unsplash.com/photos/h0Vxgz5tyXA/download?force=true&w=640"
                alt="#"
            />
            <div className="profile w-full flex m-3 ml-4 text-white">
                <img
                    className="w-28 h-28 p-1  rounded-full"
                    src={`http://127.0.0.1:8000/images/profiles/${image}`} // Assuming your user data has a profile image URL
                    alt="avatar avatr avatar"
                />
                <div className="title mt-9 ml-3 font-bold flex flex-col">
                    <div className="name text-2xl break-words">{userName}</div>
                    <div className="text-gray-800 font-semibold text-sm italic">
                        {title}
                    </div>
                    <div className="text-gray-600  break-words">
                        {ville_name}
                    </div>
                </div>
            </div>
            {!isOwnProfile ? null : (
                <div className="buttons flex absolute bottom-0 font-bold right-0 text-xs text-gray-500 space-x-0 my-3.5 mr-3">
                    <Link
                        to={`/message/${userData?.user?.id}`} className="add border rounded-l-2xl rounded-r-sm border-gray-300 p-1 px-4 cursor-pointer hover:bg-gray-700 hover:text-white">
                            Message
                    </Link>
                </div>
            )}
        </div>
    );
}
