import React, { useState, useEffect } from "react";
import defaultImage from "../../../assets/me.png"; // renamed for clarity

export default function Experience({ res_Data  }) {
    const [savedExper, setSavedExper] = useState([]);

    useEffect(() => {
        if (res_Data && res_Data.length > 0 && res_Data[0].experiences) {
            setSavedExper(res_Data[0].experiences);
        }
    }, [res_Data]);

    const shouldRender = res_Data && res_Data.length > 0;

    return (
        shouldRender && (
            <section className="experience-section">
                <div className="card border shadow-lg m-10">
                    <h2 className="text-2xl font-bold mb-4 text-center">Experience</h2>
                    {savedExper.map((exp) => (
                        <article key={exp.id} className="w-full rounded-xl p-1 shadow-lg bg-white mb-8">
                            <div className="grid grid-cols-1 gap-12 lg:grid-cols-12">
                                <div className="grid-cols-1 lg:col-span-3">
                                    <div className="mx-auto flex h-[90px] w-[90px] items-center justify-center rounded-full bg-blue-100 p-4">
                                        <img src={exp.image || defaultImage} alt="Experience Image" className="rounded-full" />
                                    </div>
                                </div>
                                <div className="col-span-1 lg:col-span-9">
                                    <div className="text-center lg:text-left">
                                        <div className="flex">
                                            <p className="text-base font-bold">Experience:</p>
                                            <span className="ml-4 font-semibold text-zinc-500">{exp.title}</span>
                                        </div>
                                        {exp.company ? (
                                            <div className="flex">
                                                <p className="text-base font-bold">Company:</p>
                                                <span className="ml-4 font-semibold text-zinc-500">{exp.company.name}</span>
                                            </div>
                                        ) : (
                                            exp.vr_company && (
                                                <div className="flex">
                                                    <p className="text-base font-bold">Company:</p>
                                                    <span className="ml-4 font-semibold text-zinc-500">{exp.vr_company}</span>
                                                </div>
                                            )
                                        )}
                                        <div className="flex">
                                            <p className="text-base font-bold">Date:</p>
                                            <span className="ml-4 font-semibold text-zinc-500">{exp.start_date} &nbsp;--&nbsp; {exp.end_date || "En cours"}</span>
                                        </div>
                                        <div className="flex">
                                            <p className="text-base font-bold">City:</p>
                                            <span className="ml-4 font-semibold text-zinc-500">{exp.ville?.name}</span>
                                        </div>
                                        <div className="flex">
                                            <p className="text-base font-bold">Description:</p>
                                            <span className="ml-4 font-semibold text-zinc-500">{exp.description}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                        </article>
                    ))}
                </div>
            </section>
        )
    );
}
