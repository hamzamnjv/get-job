import { useState, useEffect } from 'react';
import axios from 'axios';
import categories from '../../../../public/categories.json';
import languages from '../../../../public/languages.json';
import villes from '../../../../public/villes.json';

function Eprofile({ res_Data }) {
    const [userName, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [description, setDescription] = useState('');
    const [image, setImage] = useState(null);
    const [category, setCategory] = useState('');
    const [title, setTitle] = useState('');
    const [ville_name, setVille_name] = useState('');
    const [selectedLanguage, setSelectedLanguage] = useState('');
    const [userLanguages, setUserLanguages] = useState([]);
    const [userData, setUserData] = useState({});
    const [user_id, setUser_id] = useState('');
    const [pid, setPid] = useState('');
    const token = localStorage.getItem('token');

    useEffect(() => {
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            setUserData(JSON.parse(storedUserData));
        }
    }, []);

    useEffect(() => {
        if (userData && userData.user) {
            const { user } = userData;
            setUserName(user.full_name || '');
            setEmail(user.email || '');
            setUser_id(user.id || '');
        }
        console.log(res_Data)
        if (res_Data ) {
            const languagesArray = JSON.parse(res_Data[0].languages);
            const { user_id, phone, profile_title, description, image } = res_Data[0];
            setPhone(phone || '');
            setTitle(profile_title || '');
            setDescription(description || '');
            setImage(image || null);
            setUserLanguages(languagesArray || []);
            setPid( user_id|| '');
        }
    }, [userData, res_Data]);

    const handleAddLanguage = () => {
        if (selectedLanguage && !userLanguages.includes(selectedLanguage)) {
            setUserLanguages([...userLanguages, selectedLanguage]);
            setSelectedLanguage('');
        }
    };

    const handleRemoveLanguage = async (language) => {
        const updatedLanguages = userLanguages.filter(lang => lang !== language);
        setUserLanguages(updatedLanguages);

        try {
            const response = await axios.delete('http://127.0.0.1:8000/api/profiles/remove-language', {
                data: { user_id, language },
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            });
            console.log('Response:', response.data);
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('phone', phone);
        formData.append('description', description);
        formData.append('profile_title', title);
        if (image) {
            formData.append('image', image);
        }
        formData.append('ville_id', ville_name);
        formData.append('category_id', category);
        userLanguages.forEach((lang, index) => {
            formData.append(`languages[${index}]`, lang);
        });

        try {
            let response;
            if (Object.keys(res_Data).length > 0) {
                response = await axios.put(`http://127.0.0.1:8000/api/profiles/${pid}`, formData, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'multipart/form-data'
                    }
                });
            } else {
                response = await axios.post('http://127.0.0.1:8000/api/profiles/create', formData, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'multipart/form-data'
                    }
                });
            }

            console.log('Response:', response.data);
        } catch (error) {
            console.error('Error:', error);
        }
    };

    return (
        <div className="bg-white border border-4 rounded-lg shadow relative m-10">
            <div className="flex items-start justify-between p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Settings Profile</h3>
            </div>

            <div className="p-6 space-y-6">
                <form onSubmit={handleSubmit}>
                    <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="image-profile" className="text-sm font-medium text-gray-900 block mb-2">Change Profile image:</label>
                            {image && (
                                <img src={`http://127.0.0.1:8000/images/profiles/${image}`} className='w-20 h-20 rounded-full' alt="Profile" />
                            )}
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <input className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block mt-12 w-full p-2.5" id="image-profile" type="file" accept=".jpeg,.jpg,.png,.gif" onChange={(e) => setImage(e.target.files[0])} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="first-name" className="text-sm font-medium text-gray-900 block mb-2">First Name</label>
                            <input type="text" name="first-name" id="first-name" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" value={userName} onChange={(e) => setUserName(e.target.value)} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="title" className="text-sm font-medium text-gray-900 block mb-2">Title</label>
                            <input type="text" name="title" id="title" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="title" value={title} onChange={(e) => setTitle(e.target.value)} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="email" className="text-sm font-medium text-gray-900 block mb-2">Email</label>
                            <input type="text" name="email" id="email" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Email" value={email} readOnly onChange={(e) => setEmail(e.target.value)} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="phone" className="text-sm font-medium text-gray-900 block mb-2">Phone</label>
                            <input type="phone" name="phone" id="phone" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Phone number" value={phone} onChange={(e) => setPhone(e.target.value)} />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="ville_name" className="text-sm font-medium text-gray-900 block mb-2">City:</label>
                            <select value={ville_name} onChange={(e) => setVille_name(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                                <option value="">Select a city</option>
                                {villes.map((city) => (
                                    <option key={city.id} value={city.ville}>
                                        {city.ville}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="language" className="text-sm font-medium text-gray-900 block mb-2">Select a Language:</label>
                            <select name="language" id="language" value={selectedLanguage} onChange={(e) => setSelectedLanguage(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                                <option value="">Select a language...</option>
                                {languages.map(language => (
                                    <option key={language.id} value={language.name}>{language.name}</option>
                                ))}
                            </select>
                            <button type="button" onClick={handleAddLanguage} className="text-green-600 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-md text-xs px-3 py-1.5 text-center me-2 mb-2 mt-2">
                                Add Language
                            </button>

                            {Array.isArray(userLanguages) && (
                                <div className="col-span-6 sm:col-span-3">
                                    <h4 className="text-sm font-medium text-gray-900 block mb-2">Selected Languages:</h4>
                                    <ul className="list-disc list-inside">
                                        {userLanguages.map(language => (
                                            <li key={language}>
                                                {language}
                                                <button type="button" onClick={() => handleRemoveLanguage(language)} className="text-red-600 hover:text-white border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-md text-xs px-2 py-1 text-center ms-2 mb-2">
                                                    Remove
                                                </button>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            )}
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="category" className="text-sm font-medium text-gray-900 block mb-2">Category:</label>
                            <select value={category} onChange={(e) => setCategory(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                                <option value="">Select a category</option>
                                {categories.map(category => (
                                    <option key={category.id} value={category.name}>
                                        {category.name}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="description" className="text-sm font-medium text-gray-900 block mb-2">Description:</label>
                            <textarea id="description" rows="4" className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-cyan-600 focus:border-cyan-600" placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
                        </div>
                    </div>

                    <div className="pt-6">
                        <button type="submit" className="text-white bg-cyan-600 hover:bg-cyan-800 focus:ring-4 focus:outline-none focus:ring-cyan-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Eprofile;
