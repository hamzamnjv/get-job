import { useState, useEffect } from 'react';
import villes from '../../../../public/villes.json';
import axios from 'axios';
import { useSelector, useDispatch } from "react-redux";
import { updateUserData } from '../../../redux/actions';

export default function Experience({ res_Data , cpny_Data}) {
    const [formData, setFormData] = useState({
        company: '',
        vrCompany: '',
        ville_name: '',
        title: '',
        start_date: '',
        end_date: '',
        description: '',
        en_cour: false
    });
    const [showInsertCompany, setShowInsertCompany] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(false);
    const [savedExp, setSavedExp] = useState([]);
    const [cpn_Data, setCpn_Data] = useState({});
    const token = localStorage.getItem('token');
    const userData = useSelector(state => state.userData);
    const dispatch = useDispatch();

    useEffect(() => {
        if (res_Data && res_Data.length > 0 && res_Data[0].experiences) {
            setSavedExp(res_Data[0].experiences);
        }
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            const parsedUserData = JSON.parse(storedUserData);
            dispatch(updateUserData(parsedUserData));
        }
    }, [dispatch, res_Data]);

    useEffect(() => {
        const headers = { 'Authorization': `Bearer ${token}` };
        const fetchRes_Data = async () => {
            try {
                const response = await axios.get('http://127.0.0.1:8000/api/all/cpn', { headers });
                setCpn_Data(response.data);
            } catch (error) {
                console.error('Error fetching user data:', error);
            }
        };

        fetchRes_Data();
    }, [token]);

    const handleInputChange = (e) => {
        const { name, value, type, checked } = e.target;
        setFormData((prevData) => ({
            ...prevData,
            [name]: type === 'checkbox' ? checked : value
        }));
    };

    const handleInsertCompany = () => {
        setShowInsertCompany(true);
        setFormData((prevData) => ({ ...prevData, company: '' }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const { company, vrCompany, ville_name, title, start_date, end_date, description, en_cour } = formData;
        if ((!company && !vrCompany) || !ville_name || !title || !start_date || (!en_cour && !end_date) || !description) {
            setError('Please fill in all required fields.');
            return;
        }

        setLoading(true);
        setError(null);
        setSuccess(false);

        try {
            const endDate = en_cour ? null : end_date;
            const response = await axios.post('http://127.0.0.1:8000/api/expreriences/create', {
                user_id: userData.user.id,
                company: company,
                vr_company: vrCompany,
                ville_name: ville_name,
                title: title,
                start_date: start_date,
                end_date: endDate,
                description: description,
                en_cour: en_cour
            }, {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            });

            console.log('Experience data saved:', response.data);
            setSuccess(true);
            setFormData({
                company: '',
                vrCompany: '',
                ville_name: '',
                title: '',
                start_date: '',
                end_date: '',
                description: '',
                en_cour: false
            });

            // Update local saved experiences state
            setSavedExp((prevExp) => [...prevExp, response.data]);
        } catch (error) {
            if (error.response && error.response.status === 422) {
                console.error('Validation errors:', error.response.data);
                setError(`Unprocessable Content: ${JSON.stringify(error.response.data)}`);
            } else {
                setError('Error saving data. Please try again.');
            }
            console.error('Error saving data:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleDelete = async (expId) => {
        setLoading(true);
        setError(null);
        setSuccess(false);

        try {
            await axios.delete(`http://127.0.0.1:8000/api/expreriences/${expId}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });

            setSavedExp((prevExp) => prevExp.filter(exp => exp.id !== expId));
            setSuccess(true);
        } catch (error) {
            setError('Error deleting data. Please try again.');
            console.error('Error deleting data:', error);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="bg-white border border-4 rounded-lg shadow relative m-10">
            <div className="flex items-start justify-between p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Experience</h3>
            </div>
            <div className="p-6 space-y-6">
                {error && <div className="text-red-500">{error}</div>}
                {success && <div className="text-green-500">Operation completed successfully!</div>}
                <form onSubmit={handleSubmit}>
                    <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="company" className="text-sm font-medium text-gray-900 block mb-2">Company Name:</label>
                            {showInsertCompany ? (
                                <input
                                    type="text"
                                    name="vrCompany"
                                    value={formData.vrCompany}
                                    onChange={handleInputChange}
                                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                />
                            ) : (
                                <select
                                    name="company"
                                    value={formData.company}
                                    onChange={handleInputChange}
                                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                >
                                    <option value="">Select Company</option>
                                    {Array.isArray(cpn_Data) && cpn_Data.map(company => (
                                        <option key={company.id} value={company.name}>{company.name}</option>
                                    ))}
                                </select>
                            )}
                            {!showInsertCompany && (
                                <button type="button" onClick={handleInsertCompany} className="text-sm text-teal-600 hover:underline mt-1 focus:outline-none">Insert New Company</button>
                            )}
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="title" className="text-sm font-medium text-gray-900 block mb-2">Title:</label>
                            <input
                                type="text"
                                name="title"
                                value={formData.title}
                                onChange={handleInputChange}
                                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                            />
                        </div>

                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="start_date" className="text-sm font-medium text-gray-900 block mb-2">Date Start:</label>
                            <input
                                type="date"
                                name="start_date"
                                value={formData.start_date}
                                onChange={handleInputChange}
                                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                            />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="end_date" className="text-sm font-medium text-gray-900 block mb-2">Date End:</label>
                            <input
                                type="date"
                                name="end_date"
                                value={formData.end_date}
                                onChange={handleInputChange}
                                disabled={formData.en_cour}
                                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                            />
                            <label className="text-sm font-medium text-gray-900 block mb-2">
                                <input
                                    type="checkbox"
                                    name="en_cour"
                                    checked={formData.en_cour}
                                    onChange={handleInputChange}
                                    className="mr-1"
                                />
                                Currently Working Here
                            </label>
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="ville_name" className="text-sm font-medium text-gray-900 block mb-2">City:</label>
                            <select
                                name="ville_name"
                                value={formData.ville_name}
                                onChange={handleInputChange}
                                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                            >
                                <option value="">Select a city</option>
                                {villes.map((city) => (
                                    <option key={city.id} value={city.ville}>
                                        {city.ville}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="description" className="text-sm font-medium text-gray-900 block mb-2">Description:</label>
                            <textarea
                                name="description"
                                value={formData.description}
                                onChange={handleInputChange}
                                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-4"
                            />
                        </div>
                    </div>
                    <div className="p-6 border-t mt-2 border-gray-200 rounded-b">
                        <button className="text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit" disabled={loading}>
                            {loading ? 'Saving...' : 'Save'}
                        </button>
                    </div>
                </form>
            </div>

            {savedExp && savedExp.length > 0 ? (
                <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <caption className="p-5 text-lg font-semibold text-left rtl:text-right text-gray-900 bg-white dark:text-white dark:bg-gray-800">
                            ALL Experiences:
                        </caption>
                        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" className="px-6 py-3">
                                    Experience
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Company Name
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Ville 
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Delete
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {savedExp.map((exp) => (
                                <tr key={exp.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {exp.title}
                                    </td>
                                    <td className="px-6 py-4">
                                        {exp.company?.name || exp.vr_company}
                                    </td>
                                    <td className="px-6 py-4">
                                        {exp.ville?.name}
                                    </td>
                                    <td className="px-6 py-4">
                                        <button type="button" onClick={() => handleDelete(exp.id)} className="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-medium rounded-full text-sm px-5 py-2.5 text-center me-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">
                                            X
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            ) : null}
        </div>
    );
}
