/* eslint-disable react/prop-types */
import { useState, useEffect } from 'react';
import villes from '../../../../public/villes.json';
import axios from 'axios';
import { useSelector, useDispatch } from "react-redux";
import { updateUserData } from '../../../redux/actions';

export default function Eformation({res_Data}) {
    const [msg, setMsg] = useState('');
    const [name_school, setName_school] = useState('');
    const [diploma, setDiploma] = useState('');
    const [ville_name, setVille_name] = useState('');
    const [start_date, setStart_date] = useState('');
    const [end_date, setEnd_date] = useState('');
    const [description, setDescription] = useState('');
    const token = localStorage.getItem('token');
    const [savedEducation, setSavedEducation] = useState([]);
    const userData = useSelector(state => state.userData);
    const dispatch = useDispatch();

    useEffect(() => {
        if (res_Data && res_Data.length > 0 && res_Data[0].education) {
            setSavedEducation(res_Data[0].education);
        } 
        const storedUserData = localStorage.getItem('userData');
        if (storedUserData) {
            const userData = JSON.parse(storedUserData);
            dispatch(updateUserData(userData));
        }
    }, [dispatch ,res_Data]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!userData || !userData.user || !userData.user.id) {
            setMsg('User data not available. Please log in.');
            return;
        }

        if (!token) {
            setMsg('Token not available. Please log in.');
            return;
        }

        try {
            const response = await axios.post('http://127.0.0.1:8000/api/education/create', {
                user_id: userData.user.id,
                name_school: name_school,
                diploma: diploma,
                ville_name: ville_name,
                start_date: start_date,
                end_date: end_date,
                description: description
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            setMsg('Education created successfully.');
            console.log('Form data submitted:', response.data);

            setName_school('');
            setDiploma('');
            setVille_name('');
            setStart_date('');
            setEnd_date('');
            setDescription('');
        } catch (error) {
            console.error('Error submitting form data:', error);
            setMsg('Error creating education. Please try again.');
        }
    };

    const handleDelete = async (educationId) => {
        if (!token) {
            setMsg('Token not available. Please log in.');
            return;
        }
        try {
            await axios.delete(`http://127.0.0.1:8000/api/education/${educationId}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            // Update savedEducation state to reflect the deletion
            setSavedEducation(savedEducation.filter(edu => edu.id !== educationId));
            setMsg('Education deleted successfully.');
        } catch (error) {
            console.error('Error deleting education:', error);
            setMsg('Error deleting education. Please try again.');
        }
    };
    



    return (
        <div className="bg-white border border-4 rounded-lg shadow relative m-10">
            <div className="flex items-start justify-between p-5 border-b rounded-t">
                <h3 className="text-xl font-semibold">Formation</h3>
            </div>
            <div className="p-6 space-y-6">
                <form onSubmit={handleSubmit}>
                    <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="name_school" className="text-sm font-medium text-gray-900 block mb-2">University Name:</label>
                            <input type="text" value={name_school} onChange={(e) => setName_school(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="diploma" className="text-sm font-medium text-gray-900 block mb-2">Diploma:</label>
                            <input type="text" value={diploma} onChange={(e) => setDiploma(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="start_date" className="text-sm font-medium text-gray-900 block mb-2">Start Date:</label>
                            <input type="date" value={start_date} onChange={(e) => setStart_date(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="end_date" className="text-sm font-medium text-gray-900 block mb-2">End Date:</label>
                            <input type="date" value={end_date} onChange={(e) => setEnd_date(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                            <label htmlFor="ville_name" className="text-sm font-medium text-gray-900 block mb-2">City:</label>
                            <select value={ville_name} onChange={(e) => setVille_name(e.target.value)} className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                                <option value="">Select a city</option>
                                {villes.map((city) => (
                                    <option key={city.id} value={city.ville}>
                                        {city.ville}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="col-span-6">
                            <label htmlFor="description" className="text-sm font-medium text-gray-900 block mb-2">Description:</label>
                            <textarea value={description} onChange={(e) => setDescription(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-4" />
                        </div>
                    </div>
                    <div className="p-6 border-t mt-2 border-gray-200 rounded-b">
                        <button className="text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="submit">Save</button>
                    </div>
                </form>
                {msg && (
                    <div className={`p-4 mb-4 text-sm ${msg.includes('Error') ? 'text-red-800' : 'text-green-800'} rounded-lg dark:bg-gray-800 dark:text-${msg.includes('Error') ? 'red-400' : 'green-400'}`} role="alert">
                        <span className="font-medium">{msg}</span>
                    </div>
                )}
            </div>
            

            {savedEducation && savedEducation.length > 0 ? (
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
        <caption className="p-5 text-lg font-semibold text-left rtl:text-right text-gray-900 bg-white dark:text-white dark:bg-gray-800">
            ALL Foramation :
        </caption>
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" className="px-6 py-3">
                    Formation
                </th>
                <th scope="col" className="px-6 py-3">
                    University 
                </th>
                <th scope="col" className="px-6 py-3">
                    Delete
                </th>
            </tr>
        </thead>
        <tbody>
        {savedEducation.map((edu) => (
            <tr key={edu.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                {edu.diploma}
                </th>
                <td className="px-6 py-4">
                {edu.name_school}
                </td>
                <td className="px-6 py-4 ">
                <button type="button" onClick={() => handleDelete(edu.id)} className="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-medium rounded-full text-sm px-5 py-2.5 text-center me-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">
                    X</button>
                </td>
            </tr>))}
        </tbody>
    </table>
</div>

) : null}


        </div>
    );
}
