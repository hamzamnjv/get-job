/* eslint-disable react/prop-types */
import { useState, useEffect } from 'react';
import axios from 'axios';
import { useSelector } from 'react-redux';

export default function Eskills({ res_Data }) {
  const [skills, setSkills] = useState('');
  const [savedSkills, setSavedSkills] = useState([]);
  const [errorMsg, setErrorMsg] = useState('');
  const token = localStorage.getItem('token');
  const userData = useSelector(state => state.userData);
  
  useEffect(() => {
    if (res_Data && res_Data.length > 0 && res_Data[0].skill) {
      setSavedSkills(res_Data[0].skill);
    } 
  }, [res_Data]);



  const handleSubmit = async (e) => {
    e.preventDefault();
    if (skills.trim() !== '') {
      try {
        const response = await axios.post('http://localhost:8000/api/skills/create', {
          skills: skills,
          user_id: userData.user.id
        }, {
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
          },
        });
        setSavedSkills([...savedSkills, response.data]);
        setSkills('');
        setErrorMsg('');
      } catch (error) {
        console.error('Error adding skill:', error);
        console.log('Token:', token); // Log the token to verify it's correct
        if (error.response) {
          console.log('Response data:', error.response.data);
          console.log('Response status:', error.response.status);
          console.log('Response headers:', error.response.headers);
        }
        setErrorMsg('Error adding skill. Please try again.');
      }
    } else {
      setErrorMsg('Please enter a skill name');
    }
  };

  const handleDelete = async (id) => {
    if (window.confirm('Are you sure you want to delete this skill?')) {
      try {
        await axios.delete(`http://localhost:8000/api/skills/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        setSavedSkills(savedSkills.filter(skill => skill.id !== id));
      } catch (error) {
        console.error('Error deleting skill:', error);
      }
    }
  };

  return (
    <div className="bg-white border border-4 rounded-lg shadow relative m-10">
      <div className="flex items-start justify-between p-5 border-b rounded-t">
        <h3 className="text-xl font-semibold">Skills</h3>
      </div>
      <div className="p-6 space-y-6">
        <form onSubmit={handleSubmit}>
          <div className="grid grid-cols-6 gap-6">
            <div className="col-span-6 sm:col-span-3">
              <label htmlFor="skills" className="text-sm font-medium text-gray-900 block mb-2">Skill Name:</label>
              <input
                type="text"
                id="skills"
                value={skills}
                onChange={(e) => setSkills(e.target.value)}
                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
              />
            </div>
          </div>
          <div className="p-6 border-t mt-2 border-gray-200 rounded-b">
            <button
              className="text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
              type="submit"
            >
              Save
            </button>
          </div>
        </form>
        
        {errorMsg && <p className="text-red-500">{errorMsg}</p>}
        
        {savedSkills && savedSkills.length > 0 ? (
          <div className="inline-flex rounded-md shadow-sm" role="group">
            {savedSkills.map((skill) => (
              <div key={skill.id} className='mx-2'>
                <button type="button" className="px-4 py-2 text-sm font-medium text-white bg-green-600 border border-green-700 rounded-s-lg hover:bg-green-600 hover:text-white focus:z-10 focus:ring-2 focus:ring-green-700 focus:text-white dark:bg-green-700 dark:border-green-700 dark:text-white dark:hover:text-white dark:hover:bg-green-600 dark:focus:ring-green-500 dark:focus:text-white">
                  {skill.skill}
                </button>
                <button 
                  type="button" 
                  className="px-4 py-2 text-sm font-medium text-white bg-red-600 border border-red-700 rounded-e-lg hover:bg-red-700 hover:text-white focus:z-10 focus:ring-2 focus:ring-red-500 focus:bg-red-700 focus:text-white dark:border-white dark:text-white dark:hover:text-white dark:hover:bg-red-700 dark:focus:bg-red-700"
                  onClick={() => handleDelete(skill.id)}
                >
                  X
                </button>
              </div>
            ))}
          </div>
        ) : null}
        
      </div>
    </div>
  );
}
