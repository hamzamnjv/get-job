import  { useState , useEffect } from "react";
import axios from "axios";
import Editp from "./utils/editp"
import Cardprofile from "./utils/cardprofile"
import Aboutprofile from "./utils/aboutprofile";
import Experience from "./utils/experience";
import Education from "./utils/education";
import Skills from "./utils/skills";
import Contactbar from "./utils/contactbar";
import Socialbar from "./utils/socialbar";


function Profile() {

  const [res_Data, setRes_Data] = useState();
  const token =localStorage.getItem('token');
  useEffect(() => {
    
    const headers = { 'Authorization': `Bearer ${token}` };
    const fetchRes_Data = async () => {
        try {
            const response = await axios.get('http://127.0.0.1:8000/api/employee/data',{headers});
            setRes_Data(response.data);
        } catch (error) {
            console.error('Error fetching user data:', error);
        }
    };

    fetchRes_Data();

    
    return () => {
       
    };
}, [token]); 

  return (<>
    <Editp/>  
    <div className="flex flex-col lg:flex-row ">
    <div className="w-full lg:w-2/3   shadow rounded ">
    <Cardprofile res_Data={res_Data}  />
    <Aboutprofile res_Data={res_Data} />
    <Experience res_Data={res_Data} />
    <Education res_Data={res_Data}/>
    <Skills res_Data={res_Data}/>
    </div>
    <div className="w-full lg:w-1/3  shadow rounded">
    <Contactbar res_Data={res_Data}/>
    <Socialbar res_Data={res_Data}/>
    </div></div>

 </> )
}

export default Profile