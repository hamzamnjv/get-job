import  { useState } from 'react'
import { GoogleLogin } from 'react-google-login';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import imgLogin from "../../assets/logy.svg"
import Logo from "../../assets/logo.png"
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { updateUserData } from '../../redux/actions';





const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate(); 
  const dispatch = useDispatch();

  const handleLoginSuccess = async (googleData) => {
    try {
      const tokenId = googleData.tokenId;
      // Make API request to your server with the tokenId
      const response = await axios.post('/api/google-login', { tokenId });
      console.log(response.data); // Handle successful login response

      // Redirect to home page after successful login
      navigate('/blog');
    } catch (error) {
      console.error('Google login failed:', error);
    }
  };

  const handleLoginFailure = (error) => {
    console.error('Google login failed:', error);
  };

  const handleLogin = async () => {
    // Validate email using regex
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setError('Please enter a valid email address');
      return;
    }

    // Validate password (add your own validation logic)
    if (password.length < 6) {
      setError('Password must be at least 6 characters long');
      return;
    }

    try {
      // Make API request using Axios
      const response = await axios.post('http://localhost:8000/api/login', { email, password });
     
      localStorage.setItem('token', response.data.token);
      localStorage.setItem('userData', JSON.stringify(response.data));
      dispatch(updateUserData(response.data));
      console.log(response.data); 
      navigate('/blog'); 
      
    } catch (error) {
      console.error('Login failed:', error);
      setError('Login failed. Please try again.');
    }
  };

  return (
    <div className="min-h-screen bg-gray-100 text-gray-900 flex justify-center">
      <div className="max-w-screen-xl m-0 sm:m-10 bg-white shadow sm:rounded-lg flex justify-center flex-1">
        <div className="lg:w-1/2 xl:w-5/12 p-6 sm:p-12">
          <div>
            <img src={Logo} className="w-mx-auto" alt="Sign in" />
          </div>
          <div className="mt-4 flex flex-col items-center">
            <div className="w-full flex-1 mt-8">
              <div className="flex flex-col items-center">
                
                  <GoogleLogin
        className="bg-white p-2 rounded-full"
        clientId="YOUR_GOOGLE_CLIENT_ID"
        buttonText="Sign in with Google"
        onSuccess={handleLoginSuccess}
        onFailure={handleLoginFailure}
        cookiePolicy={'single_host_origin'}
      />
              </div>

              <div className="my-8 border-b text-center">
                <div className="leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform translate-y-1/2">
                  Or sign In with Cartesian E-mail
                </div>
              </div>

              <div className="mx-auto max-w-xs">
                <input
                  className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <input
                  className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5"
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {error && <div className="text-red-500">{error}</div>}
                <button
                  className="mt-5 tracking-wide font-semibold bg-green-400 text-white-500 w-full py-4 rounded-lg hover:bg-green-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none"
                  onClick={handleLogin}
                >
                  <span className="ml-4">Sign In</span>
                </button>
                <div className="text-center">
                  <Link
                    className="inline-block text-sm text-green-500 dark:text-blue-500 align-baseline hover:text-blue-800"
                    to="#"
                  >
                    Forgot Password?
                  </Link>
                </div>

                <div className="my-4 border-b text-center">
                  <div className="leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform translate-y-1/2">
                    If you do not have a compte ! :
                  </div>
                  <Link
                    to="/signup"
                    className="mt-5 bg-slate-400 text-white w-auto py-4 rounded-lg hover:bg-green-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:outline-none"
                  >
                    <span>Sign up</span>
                  </Link>
                </div>

                <p className="mt-6 text-xs text-gray-600 text-center">
                  I agree to abide by Cartesian Kinetics
                  <a href="#" className="border-b border-gray-500 border-dotted">
                    Terms of Service
                  </a>
                  and its
                  <a href="#" className="border-b border-gray-500 border-dotted">
                    Privacy Policy
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="flex-1 bg-gradient-to-r from-cyan-300 to-green-500 text-center hidden lg:flex">
          <div
            className="m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat"
            style={{ backgroundImage: `url(${imgLogin})` }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default Login;