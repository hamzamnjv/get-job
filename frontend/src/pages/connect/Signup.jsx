import  { useState } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import imgLogin from "../../assets/logy2.svg"
import Logo from "../../assets/logo.png"
import { Link } from 'react-router-dom';
// import { GoogleLogin } from 'react-google-login';






const Signup = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [role, setRole] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate(); 

  // const responseGoogle = (response) => {
  //   console.log(response);
  //   // Handle the response, extract user data, and send it to your backend for signup/authentication
  // };

 

 
  const handleSignup = async (event) => {
    event.preventDefault();
    if (!name || !email || !password || !role) {
      setError('Please fill out all fields');
      return;
    }
    // Validate email using regex
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setError('Please enter a valid email address');
      return;
    }
  
    // Validate password (add your own validation logic)
    if (password.length < 6) {
      setError('Password must be at least 6 characters long');
      return;
    }
  
    try {
      // Create a FormData object to send form data
      const formData = new FormData();
      formData.append('name', name);
      formData.append('email', email);
      formData.append('password', password);
      formData.append('role', role);
    
      // Make a POST request to your backend API endpoint for signup
      const response = await axios.post('http://127.0.0.1:8000/api/register', formData);
      console.log(response.data);
    
      navigate('/Login');
    } catch (error) {
      console.error('Signup failed:', error);
      setError('Signup failed. Please try again.');
    }
  };

  return (
    <div className="min-h-screen bg-gray-100 text-gray-900 flex justify-center">
      <div className="max-w-screen-xl m-0 sm:m-10 bg-white shadow sm:rounded-lg flex justify-center flex-1">
        <div className="lg:w-1/2 xl:w-5/12 p-6 sm:p-12">
          <div>
            <img src={Logo} className="w-mx-auto" alt="Sign in" />
          </div>
          <div className="mt-4 flex flex-col items-center">
            <div className="w-full flex-1 mt-8">
              <div className="flex flex-col items-center">
                
                  {/* <GoogleLogin
        clientId="257627059364-8chvu939o5i4os5r816ms2ln0ujek48u.apps.googleusercontent.com"
        buttonText="Sign up with Google"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
      /> */}
                
              </div>

              <div className="my-8 border-b text-center">
                <div className="leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform translate-y-1/2">
                  Or sign In with Cartesian E-mail
                </div>
              </div>
              <form onSubmit={handleSignup}>
              <div className="mx-auto max-w-xs">
              <input
                  className="w-full px-8 py-4 my-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5"
                  type="text"
                  placeholder="User Name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  autoComplete="username"
                />

                <input
                  className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  autoComplete="email"

                />
                
                <input
                  className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5"
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  autoComplete="current-password"
                />
                <select name="" id="" value={role} onChange={(e) => setRole(e.target.value)} className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5">
                  <option value='' className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white">choose your account type</option>
                  <option value="Employee">Employee</option>
                  <option value="Company">Company</option>
                </select>
                 {error && <div className="text-red-500">{error}</div>}
                <button
                  className="mt-5 tracking-wide font-semibold bg-green-400 text-white-500 w-full py-4 rounded-lg hover:bg-green-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none"
                  type='submit'
                >
                  <span className="ml-4">Sign Up</span>
                </button>


                <div className="my-4 border-b text-center">
                  <div className="leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform translate-y-1/2">
                    Already have a compte ! :
                  </div>
                  <Link
                    to="/login"
                    className="mt-5 bg-slate-400 text-white w-auto py-4 rounded-lg hover:bg-green-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:outline-none"
                  >
                    <span>Sign in</span>
                  </Link>
                </div>

                <p className="mt-6 text-xs text-gray-600 text-center">
                I agree to comply with DD205&nbsp;
                  <a href="#" className="border-b border-gray-500 border-dotted">
                    Terms of Service&nbsp;
                  </a>
                  and &nbsp;
                  <a href="#" className="border-b border-gray-500 border-dotted">
                    Privacy Policy&nbsp;
                  </a>
                </p>
              </div>
              </form>
            </div>
          </div>
        </div>
        <div className="flex-1 bg-gradient-to-r from-cyan-300 to-green-500 text-center hidden lg:flex">
          <div
            className="m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat"
            style={{ backgroundImage: `url(${imgLogin})` }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default Signup;