import  { useState, useEffect } from "react";
import axios from "axios";
import Cardcpn from "./cutils/cardcpn";
import Aboutcpn from "./cutils/aboutcpn";
import Imagecpn from "./cutils/imagecpn";
import PostList from "./cutils/postlist";
import { useParams } from "react-router-dom";

// import Teamcpn from "./cutils/teamcpn";

function Viewcompany() {
    const token = localStorage.getItem('token');
    const { id } = useParams();
    const [res_Data, setRes_Data] = useState(null);
  
    useEffect(() => {
      const headers = { 'Authorization': `Bearer ${token}` };
  
      const fetchRes_Data = async () => {
        try {
          const response = await axios.get(`http://127.0.0.1:8000/api/company/View/${id}`, { headers });
          // Assuming 'savedPosts' is the correct data field from your API response
          setRes_Data(response.data);
          localStorage.setItem('res_Data', JSON.stringify(response.data));
        } catch (error) {
          console.error('Error fetching user data:', error);
        }
      };
  
      if (id) {
        fetchRes_Data();
      }
  
      return () => {
        // Cleanup function if needed
      };
    }, [id, token]);
  
    // Conditional rendering until data is fetched
    if (!res_Data) {
      return <div>Loading...</div>;
    }

    return (
        <>
            
            <Cardcpn res_Data={res_Data} />
            <Aboutcpn res_Data={res_Data} />
            <Imagecpn res_Data={res_Data} />
            <PostList res_Data={res_Data} />
            {/* <Teamcpn res_Data={res_Data} /> */}
        </>
    );
}

export default Viewcompany;
