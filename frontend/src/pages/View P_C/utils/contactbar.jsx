import { useEffect, useState } from "react";
import { MdOutlineTranslate, MdAttachEmail } from "react-icons/md";
import { FaPhoneAlt } from "react-icons/fa";
import PropTypes from "prop-types";

export default function Contactbar({ res_Data }) {
  const [userLanguages, setUserLanguages] = useState([]);

  useEffect(() => {
    if (res_Data ) {
      const languagesArray = JSON.parse(res_Data?.languages || '[]');
      setUserLanguages(languagesArray || []);
    }
  }, [res_Data]);


  const shouldRender = res_Data ;
  
  return  shouldRender ? (
    <div className="max-w-screen w-80 mx-auto bg-white rounded-lg shadow-md overflow-hidden mt-12 lg:float-right lg:mr-12">
      <div className="bg-gray-100 px-4 py-2 lg:text-center">
        <h2 className="text-lg font-medium text-gray-800">Additional Details</h2>
      </div>
      <div className="px-4 py-5 sm:p-6">
        {res_Data.user?.email && (
          <div className="flex items-start mb-6">
            <MdAttachEmail className="text-2xl text-gray-600" />
            <span className="text-lg ml-4 font-medium text-gray-800">
              {res_Data.user.email}
            </span>
          </div>
        )}
        {res_Data.phone && (
          <div className="flex items-start mb-6">
            <FaPhoneAlt className="text-2xl text-gray-600" />
            <span className="text-lg ml-4 font-medium text-gray-800">
              {res_Data.phone}
            </span>
          </div>
        )}
{userLanguages.length > 0 && (
            <div className="flex items-start mb-6">
              <span className="text-2xl font-medium text-gray-600">
                <MdOutlineTranslate />
              </span>
              <span className="text-lg ml-4 flex list-none font-medium text-gray-800">
                {userLanguages.map(language => (
                  <li key={language}>
                    {language} &nbsp;
                  </li>
                ))}
              </span>
            </div>
          )}   </div>
    </div>
  ) : null;

}