/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";


export default function Cardprofile({res_Data }) {
    useEffect(() => {
       
    }, [ res_Data]);


    if (!res_Data) {
        return <div>Loading...</div>; 
    }

    return (
        <div className="card border hover:shadow-none relative flex flex-col shadow-lg  m-10">
            <img
                className="max-h-20 w-full opacity-80 absolute top-0"
                style={{ zIndex: -1 }}
                src="https://unsplash.com/photos/h0Vxgz5tyXA/download?force=true&w=640"
                alt="#"
            />
            <div className="profile w-full flex m-3 ml-4 text-white">
                <img
                    className="w-28 h-28 p-1  rounded-full"
                    src={`http://127.0.0.1:8000/images/profiles/${res_Data?.image}`} // Assuming your user data has a profile image URL
                    alt="avatar avatr avatar"
                />
                <div className="title mt-9 ml-3 font-bold flex flex-col">
                    <div className="name text-2xl break-words">{res_Data?.user?.full_name}</div>
                    <div className="text-gray-800 font-semibold text-sm italic">
                        {res_Data?.profile_title}
                    </div>
                    <div className="text-gray-600  break-words">
                        {res_Data?.ville?.name }
                    </div>
                </div>
            </div>
           
                <div className="buttons flex absolute bottom-0 font-bold right-0 text-xs text-gray-500 space-x-0 my-3.5 mr-3">
                    <Link
                        to={`/message/${res_Data?.user?.id}`} className="add border rounded-l-2xl rounded-r-sm border-gray-300 p-1 px-4 cursor-pointer hover:bg-gray-700 hover:text-white">
                            Message
                    </Link>
                </div>
            
        </div>
    );
}
