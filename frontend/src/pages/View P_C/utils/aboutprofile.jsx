import React from "react";
import PropTypes from "prop-types";

function Aboutpro({ res_Data }) {

 



  return (
    <div className="card border hover:shadow-none relative flex flex-col shadow-lg m-10">
      <h1 className="text-2xl font-bold mb-4 text-center">About</h1>
      {res_Data ? (
        <div>
          <p className="text-lg mx-4 mb-2 text-center">{res_Data?.description}</p>
        </div>
      ) : (
        <p className="mx-auto text-center">Loading...</p>
      )}
    </div>
  );
}



export default Aboutpro;
