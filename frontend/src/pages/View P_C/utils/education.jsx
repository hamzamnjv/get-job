/* eslint-disable react/prop-types */
import  {useState , useEffect} from "react";
import img from "../../../assets/me.png";

export default function Education({ res_Data }) {
  
    const [savedEducation, setSavedEducation] = useState([]);
    


    useEffect(() => {
            if (res_Data ) {
                setSavedEducation(res_Data.education);
                console.log(res_Data.education)

        }
    }, [ res_Data ]);
    const shouldRender =  (res_Data );

    return (
        shouldRender && (
        <div>
                <div className="card border hover:shadow-none relative flex flex-col shadow-lg m-10">
                <h2 className="text-2xl font-bold mb-4 text-center">Education</h2>
            {savedEducation.map((edu, index) => ( 
                    <div key={index}  className="w-full rounded-xl p-1 shadow-2x2 shadow-blue-200 md:w-8/12 lg:w-full bg-white">
                        <div className="grid grid-cols-1 gap-12 lg:grid-cols-12">
                            <div className="grid-cols-1 lg:col-span-3">
                                <div className="mx-auto flex h-[90px] w-[90px] items-center justify-center rounded-full bg-blue-100 p-4">
                                    <img src={img} alt="User Image" />
                                </div>
                            </div>
                            <div className="col-span-2 lg:col-span-9 ">
                                <div className="text-center lg:text-left">
                                   <div className="flex"><p className="text-base font-bold ">Univesity :</p> <span className="ml-4 font-semibold text-zinc-500"> {edu.name_school}</span></div>
                                   <div className="flex"><p className="text-base font-bold ">Date  :</p> <span className="ml-4 font-semibold text-zinc-500"> {edu.start_date}--{edu.end_date}</span></div>
                                   <div className="flex"><p className="text-base font-bold ">City :</p> <span className="ml-4 font-semibold text-zinc-500"> {edu.ville?.name}</span></div>
                                   <div className="flex"><p className="text-base font-bold ">About :</p> <span className="ml-4 w-4/5 font-semibold text-zinc-500"> {edu.description}</span></div>
                                </div>
                            </div>
                        </div>
                       <br /> <hr /><br />
                    </div> ))}
                </div>
        </div>
    ));
}
