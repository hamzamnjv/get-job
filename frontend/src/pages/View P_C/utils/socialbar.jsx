/* eslint-disable react/prop-types */
import {  useEffect } from 'react';
import { FaTwitter, FaLinkedin, FaGlobe } from 'react-icons/fa';

export default function Socialbar({ res_Data }) {


  useEffect(() => {
    
}, [res_Data]);

  const shouldRender = (res_Data);

  return (
    shouldRender && (
    <div className="max-w-screen w-80 mx-auto bg-white rounded-lg shadow-md overflow-hidden mt-12 lg:float-right lg:mr-12">
      <div className="bg-gray-100 px-4 py-2 lg:text-center">
        <h2 className="text-lg font-medium text-gray-800">Social Media </h2>
      </div>
      <div className="px-4 py-5 sm:p-6">
        {res_Data?.social_links?.linkedin && (
          <div className="flex flex-col items-start justify-between mb-6">
            <span className="text-sm font-medium text-gray-600">
              <FaLinkedin className="h-8 w-8 text-gray-600" />
            </span>
            <span className="text-sm font-medium text-gray-800">{res_Data?.social_links?.linkedin}</span>
          </div>
        )}

        {res_Data?.social_links?.twitter && (
          <div className="flex flex-col items-start justify-between mb-6">
            <span className="text-sm font-medium text-gray-600">
              <FaTwitter className="h-8 w-8 text-gray-600" />
            </span>
            <span className="text-sm font-medium text-gray-800">{res_Data?.social_links?.twitter}</span>
          </div>
        )}

        {res_Data?.social_links?.website && (
          <div className="flex flex-col items-start justify-between mb-6">
            <span className="text-sm font-medium text-gray-600">
              <FaGlobe className="h-8 w-8 text-gray-600" />
            </span>
            <span className="text-sm font-medium text-gray-800">{res_Data?.social_links?.website}</span>
          </div>
        )}
      </div>
    </div>
  ));
}
