import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import Cardprofile from "./utils/cardprofile";
import Aboutpro from "./utils/aboutprofile";
import Experience from "./utils/experience";
import Education from "./utils/education";
import Skills from "./utils/skills";
import Contactbar from "./utils/contactbar";
import Socialbar from "./utils/socialbar";

function Viewprofile() {
  const token = localStorage.getItem('token');
  const { id } = useParams();
  const [res_Data, setRes_Data] = useState(null);

  useEffect(() => {
    const headers = { 'Authorization': `Bearer ${token}` };

    const fetchRes_Data = async () => {
      try {
        const response = await axios.get(`http://127.0.0.1:8000/api/profile/View/${id}`, { headers });
        // Assuming 'savedPosts' is the correct data field from your API response
        setRes_Data(response.data);
        localStorage.setItem('res_Data', JSON.stringify(response.data));
      } catch (error) {
        console.error('Error fetching user data:', error);
      }
    };

    if (id) {
      fetchRes_Data();
    }

    return () => {
      // Cleanup function if needed
    };
  }, [id, token]);

  // Conditional rendering until data is fetched
  if (!res_Data) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <div className="flex flex-col lg:flex-row">
        <div className="w-full lg:w-2/3 shadow rounded">
          <Cardprofile res_Data={res_Data} />
          <Aboutpro res_Data={res_Data} />
          <Experience res_Data={res_Data} />
          <Education res_Data={res_Data} />
          <Skills res_Data={res_Data} />
        </div>
        <div className="w-full lg:w-1/3 shadow rounded">
          <Contactbar res_Data={res_Data} />
          <Socialbar res_Data={res_Data} />
        </div>
      </div>
    </>
  );
}

export default Viewprofile;
