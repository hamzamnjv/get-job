import React, { useState } from 'react';
import { Link } from 'react-router-dom';


export default function Teamcpn({ userdata }) {


  const [displayUsers, setDisplayUsers] = useState(3);
  const handleSeeMore = () => {
    // Increment the number of users to display by 3
    setDisplayUsers(prevDisplayUsers => prevDisplayUsers + 3);
  };

  return (
    <div className="flex justify-center mt-5">
      <div className="bg-white border border-gray-200 rounded-lg shadow hover:shadow-none shadow-lg w-3/6">
        <div>
          <h1 className="text-2xl bg-gray-200 font-bold text-center">Teams</h1>
        </div>

        <div className="mt-5 flex flex-wrap bg-white border rounded-lg shadow hover:shadow-none shadow-lg">
         {/*  {userdata.slice(0, displayUsers).map((user, index) => ( */}
            <div  /*key={index} */ className="max-w-sm my-4 mx-auto bg-white border border-gray-500 dark:bg-gray-900 rounded-lg overflow-hidden shadow-lg">
              <div className="border-b px-4 pb-6">
                <div className="text-center my-4">
                  <img className="h-32 w-32 rounded-full border-4 border-white dark:border-gray-800 mx-auto my-4" src= '{user.image}' alt="" />
                  <div className="py-2">
                    <h3 className="font-bold text-2xl text-gray-800 dark:text-white mb-1">{/*{user.name}*/}</h3>
                    <div className="inline-flex text-gray-700 dark:text-gray-300 items-center">
                     {/*  {user.category}*/}
                    </div>
                  </div>
                </div>
                <div className="flex gap-2 px-2">
                  <button className="flex-1 rounded-full bg-green-600 dark:bg-blue-800 text-white dark:text-white antialiased font-bold hover:bg-green-800 dark:hover:bg-blue-900 px-16 py-2">
                    <Link /* to= {`profile/${user.id}`}*/>View</Link>
                  </button>
                </div>
              </div>
            </div>
          {/* ))} */}
        </div>

    {/*    {displayUsers < userdata.length && (  */}
          <button onClick={handleSeeMore} className="mt-4 ml-12 text-green-700 focus:outline-none">See more...</button>
        {/* )} */}
      </div>
    </div>
  );
}
