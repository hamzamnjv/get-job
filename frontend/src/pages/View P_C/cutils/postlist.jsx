import  {  useEffect } from 'react';

const PostList = (res_Data) => {



  useEffect(() => {
    
}, [res_Data]);
  

  return (
    <div className="flex flex-col md:flex-row bg-white border border-gray-200 rounded-lg shadow mt-4 w-full max-w-screen-xl mx-auto hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">

    <div className="container mx-auto py-4">
      <h1 className="text-2xl bg-gray-200 font-bold text-center rounded-full ">List of Posts</h1>
      <div className="flex-col  md:grid-cols-2 lg:grid-cols-3 gap-4">
        {res_Data.res_Data?.posts.map(post => (
          <div key={post.id} className="bg-white my-4 w-full rounded-lg shadow-md p-4">
            <div className="flex"><p className="text-base  text-xl font-bold ">Poste :</p> <span className="ml-4 mt-1 font-semibold text-zinc-500"> {post.title} </span></div>
            <div className="flex"><p className="text-base  text-xl font-bold ">Date :</p> <span className="ml-4 mt-1 font-semibold text-zinc-500"> {post.created_at?.split('T')[0]}</span></div>
            <div className="flex"><p className="text-base  text-xl font-bold ">Category :</p> <span className="ml-4 mt-1 font-semibold text-zinc-500"> {post.category?.name}</span></div>
            <div className="flex"><p className="text-base  text-xl font-bold "> Ville:</p> <span className="ml-4 mt-1 font-semibold text-zinc-500"> {post?.ville?.name}</span></div>
            <div className="flex"><p className="text-base  text-xl font-bold ">Descrition:</p> <span className="ml-4 mt-1 font-semibold text-zinc-500"> {post.description}</span></div>
            <div className="flex"><p className="text-base  text-xl font-bold ">Salary :</p> <span className="ml-4 mt-1 font-semibold text-zinc-500"> {post.salary}</span></div>
          </div>
        ))}
      </div>
    </div>
    </div>
  );
};

export default PostList;
