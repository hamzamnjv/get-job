import imghome from '../assets/p20p.jpg';
import imghomez from '../assets/imgHomez.png';
import imghomed from '../assets/imghomed.png';
import imgho from '../assets/imghom.png';

export default function Blog() {
  return (
    <div>
        <div className="flex flex-col md:flex-row items-center justify-between bg-gradient-to-r from-yellow-200 from-10% via-sky-200 via-30% to-emerald-500 to-60% px-4 md:px-20 py-9"> 
  <div className="flex w-full items-center md:w-1/2">
    <div className="flex-grow pr-4 md:pr-8">
      <h1 className="text-3xl text-teal-500 font-bold mb-4">Welcome To GetJobs</h1>
      <p className="text-lg">
        Créez votre profil, postulez et contactez des recruteurs, professionnels et entreprises.<br/>
        GetJobs vous accompagne à chaque étape de votre recherche d emploi.
      </p>
      <div className="mt-6">

      </div>
    </div>
  </div>
  <div className="w-full  md:w-1/2 " >
    <img src={imghome} alt="Photo" className="w-full h-auto rounded-full md:h-96" />
  </div>
</div>

<div className="flex flex-col md:flex-row m-4 md:m-8">
  <div className="w-full md:w-1/2 px-4">
    <h1 className="text-3xl text-teal-500 font-bold mb-4">Our Feature</h1>
    <p className="text-gray-600">GetJobs Pages are more interactive and insightful than ever with new features like Featured posts, Sponsored Articles, Pages Messaging and more.</p>
    <img src={imghomez} alt="Left Image" className="mt-4 rounded-lg w-full" />
  </div>
  <div className="w-full md:w-1/2 px-4 flex justify-center items-center"> {/* Add flex and centering */}
    <img src={imghomed} alt="Right Image" className="mt-4 rounded-lg w-full md:w-3/4 h-auto" /> {/* Adjusted image size */}
  </div>
</div>

<div className="px-4 md:px-8 ">
  <div className="flex flex-col md:flex-row justify-between">
    <div className="w-full md:w-1/2 md:pr-4">
      <h1 className="text-3xl text-teal-500 font-bold mb-8">About</h1>
      <p className="text-gray-600 mb-8"> 
        GetJobs a débuté dans le salon du co-fondateur DEV-205 en 2024 et a été officiellement lancé le 5 mai 2003.
        Sous la direction de DEV-205, Getjobs mène une activité diversifiée et génère ses revenus à partir des souscriptions dutilisateurs,
        des ventes publicitaires et des solutions de recrutement. En décembre 2025, Microsoft a finalisé son acquisition de GetJobs,
        permettant ainsi la fusion entre le meilleur cloud professionnel au monde et le meilleur réseau professionnel au monde.
      </p>
    </div>
    <div className="w-full md:w-1/2">
      <img src={imgho} alt="Big Image" className="w-full h-auto md:pl-40 md:pr-40 rounded-lg w-full"/> 
    </div>
  </div>
</div>
        
    </div>
  )
}
