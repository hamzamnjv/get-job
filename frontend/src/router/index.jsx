import {createBrowserRouter} from "react-router-dom";

import Home from "../pages/Home";
import Login from "../pages/connect/Login";
import Signup from "../pages/connect/Signup";
import Layout from "../layouts/Layout";
import Profile from '../pages/profile_user/profile'
import Editprofile from "../pages/profile_user/editprofile";
import Cprofile from '../pages/profile_cpn/cprofile'
import Editcpnprofile from "../pages/profile_cpn/editcpnprofile";
import Postjob from "../pages/profile_cpn/postjob";
import Searchjob from "../pages/search/job";
import SearchEmplyee from "../pages/search/employee";
import SearchEntreprise from "../pages/search/entreprise";
import PageNotFound from "../components/notfound";
import Blog from "../pages/blog";
import Messages from "../pages/Messages";
import Listposted from "../pages/profile_cpn/posted";
import Viewprofile from "../pages/View P_C/viewprofile";
import Viewcompany from "../pages/View P_C/Viewcompany";

export const router = createBrowserRouter([
   
  {
    path: "/",
    element:<Home />,
  },{
    path :"*",
    element :<PageNotFound />},
   {
    path: "/login",
    element:<Login />,
  } , {
    path: "/signup",
    element:<Signup />,
  } ,{
        element: <Layout/>,
        children: [
    {
    path : '/profile',
    element : <Profile />
  },
  {
    path : "/editprofile",
    element : <Editprofile />
  },
  {
    path : "/cprofile",
    element : <Cprofile />
  },
  {
    path: "/cpnEdit",
    element : <Editcpnprofile />
  }
  ,{
    path: "/Postjob",
    element : <Postjob />
  },{
    path: "/listPost",
    element : <Listposted />
  }
  ,{
    path: "/SearchJob",
    element : <Searchjob />
  },{
    path: "/SearchEntreprise",
    element : <SearchEntreprise />
  },{
    path: "/SearchEmployee",
    element : <SearchEmplyee />
  },{
    path: "/blog",
    element : <Blog />
  },{
    path: "/messages",
    element : <Messages />
  },{
    path: "/profile/View/:id",
    element : < Viewprofile/>
  },{
    path: "/company/View/:id",
    element : < Viewcompany/>
  }

]}]);


