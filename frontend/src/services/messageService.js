import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/api';

export const getMessages = async () => {
  const response = await axios.get(`${API_URL}/messages`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  });
  return response.data;
};

export const sendMessage = async (message) => {
  const response = await axios.post(`${API_URL}/messages`, message, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  });
  return response.data;
};

export const getMessage = async (id) => {
  const response = await axios.get(`${API_URL}/messages/${id}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  });
  return response.data;
};

export const markAsRead = async (id) => {
  const response = await axios.put(`${API_URL}/messages/${id}/read`, null, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  });
  return response.data;
};
