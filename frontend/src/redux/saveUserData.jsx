import { useSelector } from 'react-redux';
import { useEffect } from 'react';

const SaveUserData = () => {
  const userData = useSelector(state => state.user.userData);

  useEffect(() => {
    if (userData) {
      localStorage.setItem('userData', JSON.stringify(userData));
    }
  }, [userData]);

  return null; // This component doesn't render anything
};

export default SaveUserData;