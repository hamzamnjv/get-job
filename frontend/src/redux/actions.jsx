import { UPDATE_USER_DATA } from './actionTypes';

export const updateUserData = (userData) => ({
  type: UPDATE_USER_DATA,
  payload: userData,
});