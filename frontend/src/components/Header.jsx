import logo from '../assets/logo.png';
import { useNavigate } from 'react-router-dom';
import { Link } from "react-router-dom";
import { Navbar,MobileNav,Typography,Button, IconButton, } from "@material-tailwind/react";
import { useDispatch, useSelector } from 'react-redux';
import { updateUserData } from '../redux/actions'
import { useEffect ,useState } from 'react';



const Header = () => {
  const [openNav, setOpenNav] = useState(false);
  const navigate = useNavigate(); 
  const userData = useSelector(state => state.userData);
  const dispatch = useDispatch();
  const [image, setImage] = useState('');

  useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpenNav(false),
    );
  }, []);

  useEffect(() => {
    const storedUserData = localStorage.getItem('userData');
    if (storedUserData) {
      const userData = JSON.parse(storedUserData);
      dispatch(updateUserData(userData));
      setImage(userData.image)
      console.log(userData)

    }
  }, [dispatch]);
  


  



    const LinkDestination = () => {
      if (userData.user.role_id === 1) {
        navigate('/dashbord')
      } else if (userData.user.role_id === 2) {
        navigate('/profile');
      } else if (userData.user.role_id === 3) {
        navigate('/cprofile');
      }
      return null;
    };
 


  const handleLogout = async () => {
    try {
      // Clear token from local storage
      localStorage.removeItem('token');
      localStorage.removeItem('userData');

      // Dispatch logout action if using Redux
      dispatch({ type: 'USER_LOGOUT' });

      // Navigate to login page
      navigate('/login');
    } catch (error) {
      console.error("Error logging out:", error);
    }
  };

 
  const navList = (
    <ul className="mt-2 mb-4 text-slate-900 flex flex-col gap-2 lg:mb-0 lg:mt-0 lg:flex-row lg:items-center lg:gap-6">
      <Typography
        as="li"
        variant="small"
        color="blue-gray"
        className="flex items-center gap-x-2 p-1 font-medium"
      >
        <Link to="/blog" className="flex items-center">
          Home
        </Link>
      </Typography>
      <Typography
        as="li"
        variant="small"
        color="blue-gray"
        className="flex items-center gap-x-2 p-1 font-medium"
      >
        <Link to='/SearchJob' className="flex items-center">
          Jobs
        </Link>
      </Typography>
      <Typography
        as="li"
        variant="small"
        color="blue-gray"
        className="flex items-center gap-x-2 p-1 font-medium"
      >
      
      <Link to='/SearchEmployee' className="flex items-center">
          Employee
        </Link>
      </Typography>
      <Typography
        as="li"
        variant="small"
        color="blue-gray"
        className="flex items-center gap-x-2 p-1 font-medium"
      >
        <Link to='/SearchEntreprise' className="flex items-center">
          Entreprise
        </Link>
      </Typography>

      <Typography
        as="li"
        variant="small"
        color="blue-gray"
        className="flex items-center gap-x-2 p-1 font-medium"
      >
        <Link to='/messages' className="flex items-center">
          Messages
        </Link>
      </Typography>
    </ul>
  );

  return (
    <Navbar className="mx-auto bg-yellow-200 max-w-full-xl px-4 py-2 lg:px-8 lg:py-3">
      <div className="container mx-auto flex items-center justify-between text-blue-900">
        
        <Typography as="a" href="/blog" className="mr-4 cursor-pointer py-1.5 font-medium" >
          <img src={logo} className="w-48 h-8"  />
        </Typography>

        <div className="hidden lg:block">{navList}</div>
        <div className="flex items-center gap-x-1">
        {userData && (
            <button onClick={LinkDestination}  className="flex hidden text-slate-900 pr-8 lg:inline-block">
              <img src={`http://127.0.0.1:8000/images/profiles/${image}`} alt="User Avatar" className="h-8 w-8 rounded-full mr-2 float-left" />
              <small className="font-bold">{userData.user.full_name}</small>
            </button>
          )}
          <Button onClick={handleLogout}
            variant="gradient"
            size="sm"
            className="hidden lg:inline-block bg-green-600"
          >
            <span>Log Out</span>
          </Button>
        </div>
        <IconButton
          variant="text"
          className="ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
          ripple={false}
          onClick={() => setOpenNav(!openNav)}
        >
          {openNav ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              className="h-6 w-6"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          )}
        </IconButton>
      </div>
      <MobileNav open={openNav}>
        <div className="container mx-auto">
          {navList}
          {userData && (
            <button onClick={LinkDestination} className="flex mb-2 text-slate-900 ">
              <img src={userData.image} alt="User Avatar" className="h-8 w-8 rounded-full mr-2 float-left" />
              <p>{userData.user.full_name}</p>
            </button>
          )}
          <div className="flex items-center gap-x-1">
         
            <Button  onClick={handleLogout} fullWidth variant="gradient" size="sm" className=" bg-green-600">
              <span>Log Out</span>
            </Button>
          </div>
        </div>
      </MobileNav>
    </Navbar>
  );
};

export default Header;
















