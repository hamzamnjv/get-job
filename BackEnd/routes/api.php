<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\MessagesController;
use App\Models\Post;
use App\Models\Company;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login'])->name('login');
Route::middleware('auth:sanctum')->post('logout', [AuthController::class, 'logout']);
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
// Posts create
Route::post('post/create', [PostController::class, 'CreatPost']);
// Profile create
Route::post('profiles/create', [ProfileController::class, 'create']);
});




// Publication create
Route::post('publication/create', [PublicationController::class, 'CreatPublication']);



 */

// -----------------------------------------------------------------------------------------

// Admin routes
// Route::middleware('auth:sanctum', 'auth.admin')->group(function () {
//     // Define admin routes here
// });
// -----------------------------------------------------------------------------------------

// Company routes
Route::middleware(['auth:sanctum', 'auth.company'])->group(function () {

    // company create
   Route::post('company/create', [CompanyController::class, 'create']);
    // Posts create
   Route::post('post/create', [PostController::class, 'CreatPost']);
    // Social Links create
   Route::post('cp_socialLinks/create', [CompanyController::class, 'SocialLinksEducation']);
   // Posts create
    Route::post('post/create', [PostController::class, 'CreatPost']);
       // Publication create
   Route::post('publication/create', [PublicationController::class, 'CreatPublication']);

    //upload_image
   Route::post('team/images', [CompanyController::class, 'upload']);
   Route::get('/get/images', [CompanyController::class, 'getTeamImages']);

   Route::get('company/data', [CompanyController::class, 'GetDataCompany']);


// list postuled
Route::get('list/posted', [PostController::class, 'getSavedPosts']);


    
});



// -----------------------------------------Message------------------------------------------------

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/messages', [MessagesController::class, 'store']);
    Route::get('/messagess', [MessagesController::class, 'index']);
    Route::get('/messages/{id}', [MessagesController::class, 'show']);
    Route::put('/messages/{id}/read', [MessagesController::class, 'markAsRead']);
});

// Get All users 
 Route::get('AllUsers', [MessagesController::class, 'GetAllUsers']);


// ----------------------------------  employee -------------------------------------------------------

Route::middleware(['auth:sanctum', 'auth.employee'])->group(function () {

    


// Get All Employee data
Route::get('employee/data', [ProfileController::class, 'CetAllDataEmployee']);
// Employee creat skils
Route::post('skills/create', [ProfileController::class, 'CreateSkills']);
    // Profile create
Route::post('profiles/create', [ProfileController::class, 'create']);
Route::put('profiles/{id}', [ProfileController::class, 'update']);
    
// skills create
// expreriences create
Route::post('expreriences/create', [ProfileController::class, 'createExperiences']);
// education create
Route::post('education/create', [ProfileController::class, 'createEducation']);
// Social Links create
Route::post('SocialLinks/create', [ProfileController::class, 'SocialLinksEducation']);
// all company data
Route::get('all/cpn', [ProfileController::class, 'getCompany']);


// Publication create
Route::post('publication/create', [PublicationController::class, 'CreatPublication']);
// postuler jobs
Route::post('user/posted', [PostController::class, 'savePosted']);



    
});

















// ----------------------------------recherche Route ---------------------//
    // Get All Posts
    Route::get('posts/all', function (Request $request) {
        try {
            // Fetch all posts with their related ville, company, and category
            $posts = Post::with('ville', 'company', 'category')->get();
    
            // Return the posts in a JSON response
            return response()->json(['allposts' => $posts], 200);
        } catch (\Exception $e) {
            // Log the error for debugging purposes
            Log::error('Failed to fetch posts', ['error' => $e->getMessage()]);
    
            // Return a JSON response with an error message
            return response()->json([
                'message' => 'Failed to fetch posts',
                'error' => $e->getMessage()
            ], 500);
        }
    });
      // filter recherche all Posts
   Route::post('recherche/posts', [PostController::class, 'FilterRecherchePosts']);

    // get all company
    Route::get('company/alls', [CompanyController::class, 'GetAllCompany']);
    // filter recherche all company
    Route::post('recherche/companys', [CompanyController::class, 'FilterRechercheCompany']);

   // get all Proffiles and filter if data existe
   Route::get('profiles/alls', [ProfileController::class, 'GetAllProfilesOrRechercheFilter']);
   Route::post('profil/alls', [ProfileController::class, 'GetAllProfilesOrRechercheFilter']);

   Route::get('profile/View/{id}', [ProfileController::class, 'viewProfileEmployee']);
   Route::get('company/View/{id}', [CompanyController::class, 'viewProfileCompany']);

