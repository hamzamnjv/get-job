<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamImage extends Model
{
    use HasFactory;

    protected $fillable = ['companies_id', 'image_path'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'companies_id');
    }
}
