<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialLinks extends Model
{
    use HasFactory;

    protected $fillable = [
        'profile_id',  	
        'website',
        'company_id',
        'facebook',
        'twitter',
        'linkedin',
        'youtube',
    ];


    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
       public function company()
    {
        return $this->belongsTo(Company::class);
    }

}
