<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Post extends Model
{
    use HasFactory;
    

    protected $fillable = [
        'title',    
        'salary',
        'ville_id',
        'description',
        'company_id',
        'category_id',
        'comment_id',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function ville()
    {
        return $this->belongsTo(Ville::class);
    }
        public function company()
    {
        return $this->belongsTo(Company::class);
    }


}
