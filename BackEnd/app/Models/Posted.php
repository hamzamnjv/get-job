<?php

namespace App\Models;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Posted extends Model
{
    protected $table = 'posted';

    protected $fillable = ['user_id', 'post_id'];


    // Define the relationship with the users table
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // Define the relationship with the posts table
    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }


    
}
