<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',  	
        'start_date',
        'end_date',
        'profile_id',
        'ville_id',
        'company_id',
        'description',
        'vr_company',
        'en_cour'
    ];


    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
    public function Company()
    {
        return $this->belongsTo(Company::class);
    }

    public function ville()
    {
        return $this->belongsTo(Ville::class);
    }
}
