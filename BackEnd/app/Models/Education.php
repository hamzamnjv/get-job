<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;


    protected $fillable = [
        'name_school', 	
        'diploma',
        'start_date',
        'end_date',
        'profile_id',
        'ville_id',
        'description',
    ];

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function ville()
    {
        return $this->belongsTo(Ville::class);
    }
}
