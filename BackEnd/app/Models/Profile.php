<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;



    protected $fillable = [							
       
        'profile_title',
        'title',
        'description',
        'image',
        'ville_id',
        'video',
        'user_id',
        'category_id',
        'phone',
        'languages'

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function socialLinks()
    {
        return $this->hasOne(SocialLinks::class);
    }

    public function experiences()
    {
        return $this->hasMany(Experience::class);
    }

    public function education()
    {
        return $this->hasMany(Education::class);
    }

    public function Skill()
    {
        return $this->hasMany(Skill::class);
    }
    public function ville()
{
    return $this->belongsTo(Ville::class, 'ville_id');
}    
public function company()
{
    return $this->belongsTo(Company::class);
}  
public function category()
{
    return $this->belongsTo(Category::class);
}  


}
