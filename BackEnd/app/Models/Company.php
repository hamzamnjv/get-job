<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [  	
        'user_id',								
        'name',       	
        'date_founded',
        'member_employees',
        'ville_id',
        'category_id',
        'industry',
        'description',
        'logo',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function Experience()
    {
        return $this->belongsTo(Experience::class);
    }

    public function ville()
    {
        return $this->belongsTo(Ville::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'industry');
    }
    
    public function socialLinks()
    {
        return $this->hasOne(SocialLinks::class);
    }
        public function posts()
    {
        return $this->hasMany(Post::class);
    }
    public function teamimages()
    {
        return $this->hasMany(TeamImage::class, 'companies_id');
    }


}
