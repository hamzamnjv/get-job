<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\Models\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = Auth::user();

        if ($user && $user->tokenCan('Token')) {
            $adminRole = Role::where('name', 'admin')->first();
    
            if ($adminRole) {
                if ($user->role_id === $adminRole->id) {
                    return $next($request);
                } else {
                    return response()->json(['message' => 'User does not have admin role'], 403);
                }
            } else {
                return response()->json(['message' => 'Admin role not found'], 500);
            }
        }
        
        return response()->json(['message' => 'Unauthorized'], 403);
   
    }
}
