<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Ville;
use App\Models\SocialLinks;
use App\Models\Category;
use App\Models\TeamImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;



use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class CompanyController extends Controller
{
    // Create Company
    public function create(Request $request)
{
    try {    						
        $request->validate([
            'user_id'  => 'required|exists:users,id',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'name' => 'nullable|string|max:255',
            'description' => 'nullable|string|max:500',
            'ville_id' => 'required|exists:villes,name',
            'industry' => 'required|exists:categories,name', 
            'date_founded' => 'nullable|date',
            'member_employees' => 'nullable|string|max:255',
        ]);
    } catch (ValidationException $e) {
        // Handle validation errors
        return response()->json(['errors' => $e->errors()], 422);
    }
     
    // Assuming the authenticated user is creating the profile
    $user = $request->user();

    try {
        // Create the profile
        $Company = new Company();
        $Company->user_id = $request->input('user_id');
        // Handle image upload if provided
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/profiles'), $imageName);
            $Company->logo = $imageName;
        }
        
        $Company->name = $request->input('name');
        $Company->description = $request->input('description');

        $villeId = Ville::where("name", $request->input('ville_id'))->value("id");
            if (!$villeId) {
                return response()->json(['message' => 'Invalid ville_id'], 422);
            }
        $Company->ville_id = $villeId;

        $industry = Category::where("name", $request->input('industry'))->value("id");
        if (!$industry ) {
            return response()->json(['message' => 'Invalid industry_id'], 422);
        }
        $Company->industry = $industry;

        $Company->date_founded = $request->input('date_founded');
        $Company->member_employees = $request->input('member_employees');
        $Company->save();
            
    } catch (\Exception $e) {
        // Handle database or other errors
        return response()->json(['message' => 'Failed to create Company'], 500);
    }

    return response()->json(['message' => 'Company created successfully'], 201);
}


    // Get All  Company
    public function getAllCompany( Company $company)
    {
        try {

            //$companies = $company->all();
            $companies = $company->with(['ville','category'])->get();

            return response()->json(['allCompanies' => $companies], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to fetch companies', 'error' => $e->getMessage()], 500);
        }
    }


    
       // Social Links create
       public function SocialLinksEducation(Request $request)
       {     													
           try {
               $validatedData = $request->validate([
                'user_id' => 'nullable|exists:users,id',
                'website' => 'nullable|url|max:255',
                'twitter' => 'nullable|url|max:255',
                'linkedin' => 'nullable|url|max:255',
                   
               ]);
           } catch (ValidationException $e) {
               // Handle validation errors
               return response()->json(['errors' => $e->errors()], 422);
           }
           
           try { 											
               // Create the experience
               $socialLinks = new SocialLinks();

               $company_id = Company::where("user_id", $validatedData['user_id'])->value("id");
               $socialLinks->company_id = $company_id;

               $socialLinks->website = $validatedData['website'];
               $socialLinks->linkedin = $validatedData['linkedin'];
               $socialLinks->twitter = $validatedData['twitter'];
               $socialLinks->save();
               
               return response()->json(['message' => 'Social Links created successfully'], 201);
           } catch (\Exception $e) {
               // Handle database or other errors
               return response()->json(['message' => 'Failed to create Social Links', 'error' => $e->getMessage()], 500);
           }
       }

    // filter recherche Company
    public function filterRechercheCompany(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'ville' => 'nullable|exists:villes,name',
                'category' => 'nullable|exists:categories,name',
                'name' => 'nullable|exists:companies,name',

            ]);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        }
    
        try {
            // Initialize query
            $query = Company::query()->with('ville','category');
    
            // Filter by name if provided
          
    
            // Get the IDs of ville and category based on their names
            if (!empty($validatedData['ville'])) {
                $villeId = Ville::where('name', $validatedData['ville'])->value('id');
                $query->where('ville_id', $villeId);
            }
    
            if (!empty($validatedData['category'])) {
                $categoryId = Category::where('name', $validatedData['category'])->value('id');
                $query->where('industry', $categoryId);
            }
            if (!empty($validatedData['name'])) {
                $companyId = Company::where('name', $validatedData['name'])->value('id');
                $query->where('id', $companyId);
             }
    
            // Execute the query
            $companies = $query->get();
    
            return response()->json(['filterRechercheCompanies' => $companies], 200);
    
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to fetch companies', 'error' => $e->getMessage()], 500);
        }
    }




    public function upload(Request $request)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ], [
            'user_id.required' => 'User ID is required.',
            'user_id.exists' => 'The specified user does not exist.',
            'image.required' => 'An image file is required.',
            'image.image' => 'The file must be an image.',
            'image.mimes' => 'The image must be a file of type: jpeg, png, jpg, gif.',
            'image.max' => 'The image size must not exceed 2MB.',
        ]);
    
        $user = Auth::user();
    
        if (!$user) {
            return response()->json(['error' => 'User not authenticated.'], 401);
        }
    
        $company = Company::where('user_id', $user->id)->value('id');
        if (!$company) {
            return response()->json(['error' => 'Company not found for the user.'], 404);
        }
    
        try {
            $file = $request->file('image');
            $imageName = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('images/team_Cpn');
    
            if (!$file->move($destinationPath, $imageName)) {
                return response()->json(['error' => 'Failed to store the image.'], 500);
            }
    
            $imagePath = 'images/team_Cpn/' . $imageName;
    
            $image = new TeamImage();
            $image->companies_id = $company;
            $image->image_path = $imagePath;
    
            if (!$image->save()) {
                return response()->json(['error' => 'Failed to save the image details.'], 500);
            }
    
            return response()->json(['message' => 'Image uploaded successfully', 'image' => $image], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => 'An error occurred while uploading the image.'], 500);
        }
    }
    

    public function getTeamImages(Request $request)
    {
        // Ensure the user is authenticated
        $user = Auth::user();

        if (!$user) {
            return response()->json(['error' => 'User not authenticated.'], 401);
        }

        // Find the company associated with the user
        $company = Company::where('user_id', $user->id)->first();

        if (!$company) {
            return response()->json(['error' => 'Company not found for the user.'], 404);
        }

        // Fetch images associated with the company
        $images = TeamImage::where('companies_id', $company->id)->get();

        if ($images->isEmpty()) {
            return response()->json(['error' => 'No images found for the company.'], 404);
        }

        $imageData = [];
        foreach ($images as $image) {
            $imagePath = public_path($image->image_path);
            if (File::exists($imagePath)) {
                $imageData[] = [
                    'id' => $image->id,
                    'image' => url($image->image_path),
                    'name' => basename($image->image_path),
                    'created_at' => $image->created_at,
                ];
            }
        }

        return response()->json(['images' => $imageData], 200);
    }

           // Get Data company
           public function GetDataCompany(Request $request)
           {  
    
            try {
                $allData = Company::with(['ville' ,'posts.ville','posts.category', 'socialLinks','teamimages','category'])
                                ->where('user_id', Auth::user()->id )
                                ->get();
    
                return response()->json($allData, 200);
            } catch (ValidationException $e) {
                return response()->json(['errors' => $e->errors()], 422);
            }
               
           }
           public function viewProfileCompany($id)
           {
            $allData = Company::with(['ville', 'category', 'socialLinks','teamimages','posts','user'])
                                ->find($id);
    
               if (!$allData) {
                   return response()->json(['error' => 'Profile not found'], 404);
               }
       
               return response()->json($allData, 200);
           }
    
                                    
    
}
