<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use App\models\Post;
use App\models\user;
use App\models\Posted;
use App\models\Ville;
use App\models\Company;
use App\models\Category;

class PostController extends Controller
{

    // Create Posts
    public function CreatPost(Request $request)
    {     								

        try {
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'title' => 'nullable|string|max:255',
                'description' => 'nullable|string|max:5000',
                'salary' => 'nullable|string|max:255',
                'ville_id' => 'required|exists:villes,name', 
                //'company_id' => 'required|exists:companies,id', 
                'category_id' => 'required|exists:categories,name', 
                //'comment_id' => 'required|exists:comments,id', 
              
            ]);
        } catch (ValidationException $e) {

            return response()->json(['errors' => $e->errors()], 422);
        }
        
        try { 											
            
            $post = new Post();

            $userId = User::where([
                ["id", $validatedData['user_id']],
                ["role_id", 3]
            ])->value("id");

            $companyId = Company::where([
                ["user_id", $userId]
            ])->value("id");
            $post->company_id = $companyId; 

            $post->title = $validatedData['title'];
            $post->description = $validatedData['description'];
            $post->salary = $validatedData['salary'];

            $villeId = Ville::where("name", $request->input('ville_id'))->value("id");
            if (!$villeId) {
                return response()->json(['message' => 'Invalid ville_id'], 422);
            }
            $post->ville_id = $villeId;

            
            $categoryId = Category::where("name", $request->input('category_id'))->value("id");
            if (!$categoryId) {
                return response()->json(['message' => 'Invalid category_id'], 422);
            }            
            $post->category_id = $categoryId;

            $post->save();
            
            return response()->json(['message' => 'Post created successfully'], 201);
        } catch (\Exception $e) {

            return response()->json(['message' => 'Failed to create post', 'error' => $e->getMessage()], 500);
        }
    }

    // Get All  Posts
   /* public function GetAllPosts(Request $request, Post $post)
    {
        try {

            $posts = $post->all();
            return response()->json(['allposts' => $posts ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to fetch posts', 'error' => $e->getMessage()], 500);
        }
    }*/

     // filter recherche Posts
     public function filterRecherchePosts(Request $request)
     {
        try {
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'title' => 'nullable|string|max:255',
                'ville' => 'nullable|exists:villes,name',
                'category' => 'nullable|exists:categories,name',
                'company' => 'nullable|exists:companies,name',
            ]);
        } catch (ValidationException $e) {
             // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        }
        try {
             // Initialize query
            $query = Post::query()->with('ville','company','category');
             // Filter by name if provided
            if (!empty($validatedData['title'])) {
                $query->where('title', $validatedData['title']);
            }
             // Get the IDs of ville and category based on their names
            if (!empty($validatedData['ville'])) {
                $villeId = Ville::where('name', $validatedData['ville'])->value('id');
                $query->where('ville_id', $villeId);
            }
            if (!empty($validatedData['category'])) {
                $categoryId = Category::where('name', $validatedData['category'])->value('id');
                $query->where('category_id', $categoryId);
            }
            if (!empty($validatedData['company'])) {
                $companyId = Company::where('name', $validatedData['company'])->value('id');
                $query->where('company_id', $companyId);
            }
             // Execute the query
                $companies = $query->get();
                return response()->json(['filterRechercheCompanies' => $companies], 200);
    
        } catch (\Exception $e) {
                return response()->json(['message' => 'Failed to fetch companies', 'error' => $e->getMessage()], 500);
        }
    }

     public function savePosted(Request $request)
    {
        // Validate the request
        $request->validate([
            'post_id' => 'required|exists:posts,id',
        ]);

        $user_id = Auth::id();
        $post_id = $request->input('post_id');

        // Check if the post is already saved
        $existingPosted = Posted::where('user_id', $user_id)
                        ->where('post_id', $post_id)->first();

        if (!$existingPosted) {
            // Save the post to the user's list
            Posted::create([
                'user_id' => $user_id,
                'post_id' => $post_id,
            ]);

            return response()->json(['message' => 'Post saved successfully.']);
        }

        return response()->json(['message' => 'Post already saved.'], 400);
    }

    public function getSavedPosts()
    {
        // Check if the user is authenticated
        if (!Auth::check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        // Get the company associated with the authenticated user
        $company = Company::where('user_id', Auth::user()->id)->first();
    
        // Check if the company exists
        if (!$company) {
            return response()->json(['error' => 'Company not found'], 404);
        }

        // Retrieve saved posts associated with the company
        $savedPosts = Posted::with('user.profile', 'post.company')
                            ->whereHas('post', function ($query) use ($company) {
                                $query->where('company_id', $company->id);
                            })
                            ->get();
    
        // Return the response
        return response()->json(['savedPosts' => $savedPosts], 200);
    }
    
}
