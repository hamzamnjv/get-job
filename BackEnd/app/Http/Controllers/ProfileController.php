<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\models\Profile;
use App\models\Company;
use App\models\Skill;
use App\models\Experience;
use App\models\Ville;
use App\models\Education;
use App\models\SocialLinks;
use Illuminate\Support\Facades\Auth; 
use App\Models\Publication;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    // Create Profile
    public function create(Request $request)
    {
        // Validate the incoming request data
        try {
            $request->validate([
                'user_id'  => 'required|exists:users,id', // Ensure the user_id exists in users table
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
                'profile_title' => 'nullable|string|max:255',
                'description' => 'nullable|string|max:500',
                'ville_id' => 'required|exists:villes,name',
                'category_id' => 'required|exists:categories,name',
                'phone' => 'nullable|string|max:20',
                'languages'    => 'nullable|array',
                'languages.*'  => 'string|max:255',
            ]);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        }
    
        // Assuming the authenticated user is creating the profile
        $user = $request->user();
    
        // Create the profile
        try {
            $profile = new Profile();
            $profile->user_id = $request->input('user_id');
    
            // Handle image upload if provided
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/profiles'), $imageName);
                $profile->image = $imageName;
            }
    
            $profile->profile_title = $request->input('profile_title');
            $profile->description = $request->input('description');
    
            // Get the ID of the ville by its name
            $villeId = Ville::where("name", $request->input('ville_id'))->value("id");
            if (!$villeId) {
                return response()->json(['message' => 'Invalid ville_id'], 422);
            }
            $profile->ville_id = $villeId;
    
            // Get the ID of the category by its name
            $categoryId = Category::where("name", $request->input('category_id'))->value("id");
            if (!$categoryId) {
                return response()->json(['message' => 'Invalid category_id'], 422);
            }
            $profile->category_id = $categoryId;
    
            $profile->phone = $request->input('phone');
            $profile->languages = json_encode($request->input('languages')); // Serialize languages array to JSON
    
            // Save the profile to the database
            $profile->save();
    
        } catch (\Exception $e) {
            // Handle database or other errors
            return response()->json(['message' => 'Failed to create profile: ' . $e->getMessage()], 500);
        }
    
        return response()->json(['message' => 'Profile created successfully'], 201);
    }
    
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'user_id' => 'required|exists:users,id',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
                'profile_title' => 'nullable|string|max:255',
                'description' => 'nullable|string|max:500',
                'ville_id' => 'nullable|exists:villes,name',
                'category_id' => 'nullable|exists:categories,name',
                'phone' => 'nullable|string|max:20',
                'languages' => 'nullable|array',
                'languages.*' => 'string|max:255',
            ]);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }

        $profile = Profile::find($id);

        if (!$profile) {
            return response()->json(['message' => 'Profile not found'], 404);
        }

        $user = $request->user();
        
        try {
            $profile->user_id = $request->input('user_id');

            if ($request->hasFile('image')) {
                if ($profile->image && file_exists(public_path('images/profiles/' . $profile->image))) {
                    unlink(public_path('images/profiles/' . $profile->image));
                }

                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/profiles'), $imageName);
                $profile->image = $imageName;
            }

            $profile->profile_title = $request->input('profile_title');
            $profile->description = $request->input('description');

            $villeId = Ville::where("name", $request->input('ville_id'))->value("id");
            if (!$villeId) {
                return response()->json(['message' => 'Invalid ville_id'], 422);
            }
            $profile->ville_id = $villeId;

            $categoryId = Category::where("name", $request->input('category_id'))->value("id");
            if (!$categoryId) {
                return response()->json(['message' => 'Invalid category_id'], 422);
            }
            $profile->category_id = $categoryId;

            $profile->phone = $request->input('phone');
            $profile->languages = json_encode($request->input('languages'));

            $profile->save();

        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to update profile: ' . $e->getMessage()], 500);
        }

        return response()->json(['message' => 'Profile updated successfully'], 200);
    }
 
    // Get All  Profiles and filter if data existe
    public function GetAllProfilesOrRechercheFilter(Request $request)
    {
        try {
            // Validate the request input
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'title' => 'nullable|string|max:255',
                'ville' => 'nullable|exists:villes,name',
                'category' => 'nullable|exists:categories,name',
                'user_name' => 'nullable|exists:users,full_name',
            ]);
        } catch (ValidationException $e) {
            // Return validation error response
            return response()->json(['errors' => $e->errors()], 422);
        }
    
        try {
            // Initialize the query with eager loading for related entities
            $query = Profile::query()->with('ville', 'company', 'category','user');
    
            // Apply filters based on the validated input
            if (!empty($validatedData['title'])) {
                $query->where('profile_title', $validatedData['title']);
            }
    
            if (!empty($validatedData['user_name'])) {
                $userid = User::where('full_name', $validatedData['user_name'])->value('id');
                if ($userid) {
                    $query->where('user_id', $userid);
                }
            }
    
            if (!empty($validatedData['ville'])) {
                $villeId = Ville::where('name', $validatedData['ville'])->value('id');
                if ($villeId) {
                    $query->where('ville_id', $villeId);
                }
            }
    
            if (!empty($validatedData['category'])) {
                $categoryId = Category::where('name', $validatedData['category'])->value('id');
                if ($categoryId) {
                    $query->where('category_id', $categoryId);
                }
            }
    
            // Retrieve the profiles based on the query
            $profiles = $query->get();
    
            // Return a successful response with the retrieved profiles
            return response()->json(['filterPostsAll' => $profiles], 200);
    
        } catch (\Exception $e) {
            // Return a failure response with the error message
            return response()->json(['message' => 'Failed to fetch profiles', 'error' => $e->getMessage()], 500);
        }
    }
    

    //   Create Skills
    public function CreateSkills(Request $request)
    {

        try {
            // Validate the request
            $validatedData = $request->validate([
                'user_id'  => 'required|exists:users,id',
                'skills' => 'nullable|string|max:255',
            ]);
    
            // Create the skill
            $skill = new Skill();
            $profileId = Profile::where("user_id", $validatedData['user_id'])->value("id");
            $skill->profile_id = $profileId;
            $skill->skill = $validatedData['skills'];
            $skill->save();
    
            return response()->json(['message' => 'Skill created successfully'], 201);
        } catch (\Exception $e) {
            // Handle errors
            return response()->json(['message' => 'Failed to create skill', 'error' => $e->getMessage()], 500);
        }
    }

    // Create Experiences
    public function createExperiences(Request $request)
    {     						
        try {
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'title' => 'nullable|string|max:255',
                'description' => 'nullable|string|max:500',
                'company' => 'nullable|exists:companies,name',
                'ville_name' => 'required|exists:villes,name',
                'start_date' => 'nullable|date', // Specify date format explicitly
                'end_date' => 'nullable|date',
                'en_cour' => 'nullable|boolean',
                'vr_company'=>'nullable|string'
                
            ]);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        }
        
        try { 											
            // Create the experience
            $experience = new Experience();
            $profileId = Profile::where("user_id", $validatedData['user_id'])->value("id");
            $experience->profile_id = $profileId;
            $experience->title = $validatedData['title'];
            $experience->description = $validatedData['description'];
            
            $vileId = Ville::where("name", $validatedData['ville_name'])->value("id");
            $experience->ville_id = $vileId;
            
            $companyId = Company::where("name", $validatedData['company'])->value("id");
            $experience->company_id = $companyId;
            
            $experience->vr_company = $validatedData['vr_company'];
             
            $experience->start_date = $validatedData['start_date'];
            $experience->end_date = $validatedData['end_date'];
            $experience->en_cour = $validatedData['en_cour'];
            $experience->save();
            
            return response()->json(['message' => 'Experience created successfully'], 201);
        } catch (\Exception $e) {
            // Handle database or other errors
            return response()->json(['message' => 'Failed to create experience', 'error' => $e->getMessage()], 500);
        }
    }

     // Create Formation or Education
     public function createEducation(Request $request)
     {     						
         try {
             $validatedData = $request->validate([
                 'user_id' => 'required|exists:users,id',
                 'name_school' => 'nullable|string|max:255',
                 'description' => 'nullable|string|max:500',
                 'diploma' => 'nullable|string|max:255',
                 'ville_name' => 'required|exists:villes,name',
                 'start_date' => 'nullable|date', // Specify date format explicitly
                 'end_date' => 'nullable|date', // Specify date format explicitly
                 
             ]);
         } catch (ValidationException $e) {
             // Handle validation errors
             return response()->json(['errors' => $e->errors()], 422);
         }
         
         try { 											
             // Create the experience
             $education = new Education();
             $profileId = Profile::where("user_id", $validatedData['user_id'])->value("id");
             $education->profile_id = $profileId;
             $education->name_school = $validatedData['name_school'];
             $education->description = $validatedData['description'];
             $vileId = Ville::where("name", $validatedData['ville_name'])->value("id");
             $education->ville_id = $vileId;
             $education->diploma = $validatedData['diploma'];
             $education->start_date = $validatedData['start_date'];
             $education->end_date = $validatedData['end_date'];
             $education->save();
             
             return response()->json(['message' => 'Education created successfully'], 201);
         } catch (\Exception $e) {
             // Handle database or other errors
             return response()->json(['message' => 'Failed to create education', 'error' => $e->getMessage()], 500);
         }
     }


       // Social Links create
       public function SocialLinksEducation(Request $request)
       {     													
           try {
               $validatedData = $request->validate([
                'user_id' => 'nullable|exists:users,id',
                'website' => 'nullable|url|max:255',
                'twitter' => 'nullable|url|max:255',
                'linkedin' => 'nullable|url|max:255',
                   
               ]);
           } catch (ValidationException $e) {
               // Handle validation errors
               return response()->json(['errors' => $e->errors()], 422);
           }
           
           try { 											
               // Create the experience
               $socialLinks = new SocialLinks();

               $profileId = Profile::where("user_id", $validatedData['user_id'])->value("id");
               $socialLinks->profile_id = $profileId;

               $socialLinks->website = $validatedData['website'];
               $socialLinks->linkedin = $validatedData['linkedin'];
               $socialLinks->twitter = $validatedData['twitter'];
               $socialLinks->save();
               
               return response()->json(['message' => 'Social Links created successfully'], 201);
           } catch (\Exception $e) {
               // Handle database or other errors
               return response()->json(['message' => 'Failed to create Social Links', 'error' => $e->getMessage()], 500);
           }
       }

       // Get All Data Employee
       public function CetAllDataEmployee(Request $request)
       {  

        try {
            $allData = Profile::with(['experiences.company','experiences.ville', 'skill', 'education.ville','ville' , 'socialLinks','company'])
                            ->where('user_id', Auth::user()->id )
                            ->get();

            return response()->json($allData, 200);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
           
       }


       public function getCompany(Request $request): \Illuminate\Http\JsonResponse
       {
           try {
               // Fetch company data
               $allData = Company::select('id', 'name')->get();
               
               // Return a successful JSON response
               return response()->json($allData, 200);
               
           } catch (ValidationException $e) {
               // Return a response with validation errors
               return response()->json(['errors' => $e->errors()], 422);
               
           } catch (\Exception $e) {
               // Log and return a generic error response
               \Log::error($e->getMessage());
               return response()->json(['error' => 'Something went wrong!'], 500);
           }
       }
    



       public function viewProfileEmployee($id)
       {
        $allData = Profile::with(['ville','experiences.company','experiences.ville', 'skill', 'education.ville' , 'socialLinks','user'])
                            ->find($id);

           if (!$allData) {
               return response()->json(['error' => 'Profile not found'], 404);
           }
   
           return response()->json($allData, 200);
       }
}
