<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publication;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class PublicationController extends Controller
{
     // Create Posts
     public function CreatPublication(Request $request)
     {
         try {
             $validator = Validator::make($request->all(), [
                 'user_id' => 'required|exists:users,id',
                 'note' => 'nullable|string|max:500',
                 'image' => 'nullable|image|max:10240', 
                 'video' => 'nullable|file|mimes:mp4,mov,avi|max:102400', 
                 'file' => 'nullable|file|max:102400', 
             ]);
     
             if ($validator->fails()) {
                 throw new ValidationException($validator);
             }
     
             $publication = new Publication();
     
             $userId = $request->input('user_id');
             $publication->user_id = $userId;
             $publication->note = $request->input('note');
     
             // Handle image upload if provided
             if ($request->hasFile('image')) {
                 $image = $request->file('image');
                 $imageName = time() . '_' . $image->getClientOriginalName();
                 $image->move(public_path('uploads/images'), $imageName);
                 $publication->image = $imageName;
             }
     
             // Handle video upload if provided
             if ($request->hasFile('video')) {
                 $video = $request->file('video');
                 $videoName = time() . '_' . $video->getClientOriginalName();
                 $video->move(public_path('uploads/videos'), $videoName);
                 $publication->video = $videoName;
             }
     
             // Handle file upload if provided
             if ($request->hasFile('file')) {
                 $file = $request->file('file');
                 $fileName = time() . '_' . $file->getClientOriginalName();
                 $file->move(public_path('uploads/files'), $fileName);
                 $publication->file = $fileName;
             }
     
             $publication->save();
     
             return response()->json(['message' => 'Publication created successfully'], 201);
         } catch (\Exception $e) {
             return response()->json(['message' => 'Failed to create publication', 'error' => $e->getMessage()], 500);
         }
     }
}
