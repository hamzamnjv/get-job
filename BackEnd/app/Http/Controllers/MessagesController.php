<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Role;
use App\Models\User;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class MessagesController extends Controller
{
    public function GetAllUsers()
    {
        try {
            // Fetch users with roles "employee" and "company"
            $users = User::with('role')
                         ->whereHas('role', function($query) {
                             $query->whereIn('name', ['Employee', 'Company']);
                         })
                         ->select('id', 'full_name', 'role_id')
                         ->get()
                         ->groupBy(function($item) {
                             return $item->role->name;
                         });
    
            // Prepare the response
            $response = [
                'employees' => $users->get('Employee', collect()),
                'companies' => $users->get('Company', collect()),
            ];
    
            return response()->json($response, 200);
        } catch (\Exception $e) {
            Log::error('Error fetching users: ' . $e->getMessage(), ['exception' => $e]);
            return response()->json(['error' => 'There was an error fetching the users'], 500);
        }
    }
    
    
    

    public function index()
    {
        try {
            // Ensure the user is authenticated
            $user = Auth::user();
            if (!$user) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            $messages = Message::where('sender_id', $user->id)
                               ->orWhere('receiver_id', $user->id)
                               ->orderBy('created_at', 'asc') 
                               ->get();

                return response()->json($messages, 200);
        } catch (\Exception $e) {

            \Log::error('Failed to retrieve messages', ['error' => $e->getMessage()]);

            return response()->json(['errors' => 'Internal Server Error'], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'receiver_id' => 'required|exists:users,id',
                'content' => 'nullable|string',
                'file' => 'nullable|file',
            ]);
    
            $filePath = null;
            if ($request->hasFile('file')) {
                $filePath = $request->file('file')->store('uploads', 'public');
            }
    
            $message = Message::create([
                'sender_id' => Auth::id(),
                'receiver_id' => $request->receiver_id,
                'content' => $request->content,
                'file' => $filePath,
            ]);
    
            return response()->json($message, 201);
        } catch (\Exception $e) {
            // Catching \Exception instead of ValidationException to handle all possible exceptions
            return response()->json(['message' => 'Failed to creat message: ' . $e->getMessage()], 500);
        }
    }
    
    

    public function show($id)
    {
        $message = Message::findOrFail($id);

        if (Auth::id() !== $message->sender_id && Auth::id() !== $message->receiver_id) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        return response()->json($message);
    }

    public function markAsRead($id)
    {
        $message = Message::findOrFail($id);

        if (Auth::id() !== $message->receiver_id) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        $message->update(['read_at' => now()]);

        return response()->json($message);
    }


}
