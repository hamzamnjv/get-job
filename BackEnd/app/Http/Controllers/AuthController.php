<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    // In your Laravel controller
public function index()
{
    // Your logic here

    // Serve CSRF token in response header
    return response()->json([
        // Other data you want to send
        'csrf_token' => csrf_token(),
    ]);
}

    
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'role' => 'required|string|exists:roles,name'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $role = Role::where('name', $request->role)->first();
        $user = User::create([
            'full_name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id' => $role->id 
        ]);

        $token = $user->createToken('AuthToken')->plainTextToken;

        return response()->json(['token' => $token], 201);
    }



    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('Token')->plainTextToken;
            if($user->role_id === 2){
            $profile = Profile::where('user_id', $user->id)->value('image');
            }else if($user->role_id === 3){
            $profile = Company::where('user_id', $user->id)->value('logo');
            }else {
                return null;
            }
            
            return response()->json([
                'user' => $user,
                'token' => $token,
                'image' => $profile,
            ], 200);
        }
        
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json(['message' => 'Successfully logged out'], 200);
    }
    
}
