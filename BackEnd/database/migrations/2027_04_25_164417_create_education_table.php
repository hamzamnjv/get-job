<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('education', function (Blueprint $table) {
            $table->id();
            $table->string('name_school');
            $table->string('diploma');
            $table->date('start_date');
            $table->date('end_date');
            $table->unsignedBigInteger('profile_id');
            $table->unsignedBigInteger('ville_id')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
    
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->foreign('ville_id')->references('id')->on('villes')->onDelete('set null');
       
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('education');
    }
};
