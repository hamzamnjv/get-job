<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('skills', function (Blueprint $table) {
            if (!Schema::hasColumn('skills', 'profile_id')) {
                $table->unsignedBigInteger('profile_id');
                $table->foreign('profile_id')->references('id')->on('profiles');
            }
        });
    }

    public function down()
    {
        Schema::table('skills', function (Blueprint $table) {
            if (Schema::hasColumn('skills', 'profile_id')) {
                $table->dropForeign(['profile_id']);
                $table->dropColumn('profile_id');
            }
        });
    }
};
