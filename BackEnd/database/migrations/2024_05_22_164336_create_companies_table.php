<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('date_founded');
            $table->integer('member_employees');
            $table->unsignedBigInteger('ville_id')->nullable();
            $table->string('industry');
            $table->text('description');
            $table->string('logo')->nullable();
            $table->timestamps();
    
            $table->foreign('ville_id')->references('id')->on('villes')->onDelete('set null');
       
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
